﻿namespace ITUtil.Common.Command.Interfaces
{
    public interface IDtoRequest
    {
        void Validate();
    }
}