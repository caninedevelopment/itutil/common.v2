﻿using System;

namespace ITUtil.Common
{
    [Serializable]
    public abstract class LocalizedException : Exception
    {
        //        public ErrorCodes ErrorCode { get; set; }
        public LocalizedString Error { get; internal set; }

        protected LocalizedException() { }
        protected LocalizedException(string message) : base(message) { }
        protected LocalizedException(string message, Exception inner) : base(message, inner) { }
        protected LocalizedException(string message, LocalizedString error) : this(message)
        {
            this.Error = error;
            //       this.ErrorCode = ErrorCodes.ServerError;
        }
    }

    public class ElementAlreadyExistsException : LocalizedException
    {
        public ElementAlreadyExistsException(string message, string itemId) : base(message)
        {
            Error = LocalizedString.ItemAlreadyExists(itemId);
            //    this.ErrorCode = ErrorCodes.Conflict;
        }

        protected ElementAlreadyExistsException() : base()
        {
        }

        protected ElementAlreadyExistsException(string message) : base(message)
        {
        }

        protected ElementAlreadyExistsException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ElementAlreadyExistsException(string message, LocalizedString error) : base(message, error)
        {
        }
    }
    public class ElementDoesNotExistException : LocalizedException
    {
        public ElementDoesNotExistException(string message, string itemId) : base(message)
        {
            Error = LocalizedString.ItemDoesNotExist(itemId);
            //    this.ErrorCode = ErrorCodes.RessourceNotFound;
        }
    }
    public class NullOrDefaultException : LocalizedException
    {
        /// <summary>
        /// propertyName should be filled out by using "nameof()"
        /// </summary>
        /// <param name="propertyName">please use nameof()</param>
        public NullOrDefaultException(string propertyName) : base("A value should be provided for the property " + propertyName)
        {
            Error = LocalizedString.NullOrDefault(propertyName);
            // this.ErrorCode = ErrorCodes.BadRequest;
        }

        public NullOrDefaultException(string message, string propertyName) : base(message)
        {
            Error = LocalizedString.NullOrDefault(propertyName);
            // this.ErrorCode = ErrorCodes.BadRequest;
        }

        protected NullOrDefaultException() : base()
        {
        }

        protected NullOrDefaultException(string message, Exception inner) : base(message, inner)
        {
        }

        protected NullOrDefaultException(string message, LocalizedString error) : base(message, error)
        {
        }
    }

    public class ValidationException : LocalizedException
    {
        /// <summary>
        /// propertyName should be filled out by using "nameof()"
        /// </summary>
        /// <param name="propertyName">please use nameof()</param>
        public ValidationException(string propertyName) : base("A valid value should be provided for the property " + propertyName)
        {
            Error = LocalizedString.ValidationError(propertyName);
            // this.ErrorCode = ErrorCodes.BadRequest;
        }

        public ValidationException(string message, string propertyName) : base(message)
        {
            Error = LocalizedString.ValidationError(propertyName);
            //  this.ErrorCode = ErrorCodes.BadRequest;
        }

        protected ValidationException() : base()
        {
        }

        protected ValidationException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ValidationException(string message, LocalizedString error) : base(message, error)
        {
        }
    }
}
