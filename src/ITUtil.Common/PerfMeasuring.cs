﻿using System.Collections.Generic;
using System.Text;

namespace ITUtil.Common
{
    public class PerfMeasuring
    {
        private readonly Dictionary<string, System.Diagnostics.Stopwatch> log;
        private bool IsActive { get; }

        public PerfMeasuring(bool isActive)
        {
            this.log = new Dictionary<string, System.Diagnostics.Stopwatch>();
            this.IsActive = isActive;
        }

        public override string ToString()
        {
            if (IsActive)
            {
                StringBuilder sb = new StringBuilder("Reported @" + System.DateTime.Now.ToString());
                foreach (var item in log)
                {
                    sb.Append(item.Key).Append(" : ").Append(item.Value.ElapsedMilliseconds).AppendLine(" ms");
                }

                return sb.ToString();
            }
            else
            {
                return "";
            }
        }

        public void StartMeasurement(string Tag)
        {
            if (IsActive)
            {
                log.Add(Tag, System.Diagnostics.Stopwatch.StartNew());
            }
        }

        public void StopMeasurement(string Tag)
        {
            if (IsActive)
            {
                if (log.ContainsKey(Tag))
                {
                    log[Tag].Stop();
                }
            }
        }
    }
}
