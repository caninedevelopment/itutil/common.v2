﻿// <copyright file="IResource.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Infrastructure.Console
{
    using ITUtil.Common.Command.Interfaces;
    using ITUtil.Infrastructure.Command;
    using System.Collections.Generic;

    public interface IResource
    {
        string Name { get; }
        List<CommandRestRegistration> Commands { get; }
        List<IEventDefinitionBase> Events { get; }
        ProgramVersion ProgramVersion { get; }

        void AddCommand<TCommandModelIn, TCommandModelOut>(IFunction<TCommandModelIn, TCommandModelOut> command, ITUtilOperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest where TCommandModelOut : IDtoResponse;

        void AddCommand<TCommandModelOut>(IFunction<TCommandModelOut> command, ITUtilOperationType operationType, string name, string description, params Claim[] claims) where TCommandModelOut : IDtoResponse;
        void AddCommand<TCommandModelIn>(IProcedure<TCommandModelIn> command, ITUtilOperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest;
        void AddCommand(IProcedure command, ITUtilOperationType operationType, string name, string description, params Claim[] claims);
    }
}
