﻿// <copyright file="ProgramVersion.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Infrastructure.Console
{
    public class ProgramVersion
    {
        public ProgramVersion()
        {
        }

        public ProgramVersion(int major/*, int minor, int revision, int build*/)
        {
            this.Major = major;
            /*this.minor = minor;
            this.minor = minor;
            this.revision= revision;
            this.build= build;*/
        }

        //public enum VersionLevel
        //{
        //    na = -1,
        //    major = 0,
        //    minor = 1,
        //    revision = 2,
        //    build = 3
        //}

        public string Version
        {
            get
            {
                return string.Format($"{this.Major}"); //.{this.minor}.{this.revision}.{this.build}");
            }

            set
            {
                this.Major = int.Parse(value);

                //                string[] parts = value.Split('.');
                //                this.major = int.Parse(parts[0]);
                //                this.minor = parts.Length > 1 ? int.Parse(parts[1]) : 0;
                //                this.revision = parts.Length > 2 ? int.Parse(parts[2]) : 0;
                //                this.build = parts.Length > 3 ? int.Parse(parts[3]) : 0;
            }
        }

        public int Major { get; set; }

        //public int minor { get; set; }

        //public int revision { get; set; }

        //public int build { get; set; }

        //public bool IsNewer(string version)
        //{
        //    ProgramVersion otherVersion = new ProgramVersion(version);
        //    return this.IsNewer(otherVersion);
        //}

        //public bool IsNewer(ProgramVersion otherVersion)
        //{
        //    if (this.major > otherVersion.major)
        //    {
        //        return true;
        //    }

        //    //if (this.major < otherVersion.major)
        //    //{
        //    //    return false;
        //    //}

        //    //if (this.minor > otherVersion.minor)
        //    //{
        //    //    return true;
        //    //}

        //    //if (this.minor < otherVersion.minor)
        //    //{
        //    //    return false;
        //    //}

        //    //if (this.revision > otherVersion.revision)
        //    //{
        //    //    return true;
        //    //}

        //    //if (this.revision < otherVersion.revision)
        //    //{
        //    //    return false;
        //    //}

        //    //if (this.build > otherVersion.build)
        //    //{
        //    //    return true;
        //    //}

        //    //if (this.build < otherVersion.build)
        //    //{
        //    //    return false;
        //    //}

        //    return false;
        //}

        //public bool IsNewer(string version, VersionLevel level)
        //{
        //    ProgramVersion otherVersion = new ProgramVersion(version);
        //    return this.IsNewer(otherVersion, level);
        //}

        //public bool IsNewer(ProgramVersion otherVersion, VersionLevel level)
        //{
        //    if (level == VersionLevel.build)
        //    {
        //        return this.IsNewer(otherVersion);
        //    }

        //    if (level == VersionLevel.revision)
        //    {
        //        this.build = 0;
        //        otherVersion.build = 0;
        //        return this.IsNewer(otherVersion);
        //    }

        //    if (level == VersionLevel.minor)
        //    {
        //        this.build = 0;
        //        otherVersion.build = 0;
        //        this.revision = 0;
        //        otherVersion.revision = 0;
        //        return this.IsNewer(otherVersion);
        //    }

        //    if (level == VersionLevel.major)
        //    {
        //        this.build = 0;
        //        otherVersion.build = 0;
        //        this.revision = 0;
        //        otherVersion.revision = 0;
        //        this.minor = 0;
        //        otherVersion.minor = 0;
        //        return this.IsNewer(otherVersion);
        //    }

        //    return false;
        //}
    }
}
