﻿using ITUtil.Common.Command;
using ITUtil.Infrastructure.RabbitMQ;
using ITUtil.Infrastructure.RabbitMQ.Common.Event;
using ITUtil.Infrastructure.RabbitMQ.Event;
using ITUtil.Infrastructure.RabbitMQ.Message;
using ITUtil.Infrastructure.RabbitMQ.Request;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITUtil.Infrastructure.Console
{
    public static class Program
    {
        public static void StartRabbitMq(List<IResource> namespaces, bool diagnostics = false)
        {
            StartRabbitMq(namespaces, null, diagnostics);
        }

        public static Task StartRabbitMq(List<IResource> resources, List<IEventListener> eventListeners, bool diagnostics = false)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            // start the request server (for handling incomming requests)
            List<MessageQueueConfiguration> configurations = new List<MessageQueueConfiguration>();

            if (eventListeners != null)
            {
                foreach (var Instance in eventListeners)
                {
                    configurations.Add(new EventConfiguration(
                    Instance.ServiceName.ToLower(),
                    new List<string>() { Instance.ServiceName.ToLower() + ".#" },
                    new Constants.EventHandler(Instance.Handler)
                    ));
                }
            }

            foreach (var Resource in resources)
            {
                var servicename = Resource.Name.ToLower();// Namespace.GetType().Name.ToLower();

                var serviceMessageHandler = new ServiceMessageHandlers(servicename, Resource);
                configurations.Add(new RequestConfiguration(//for handling routes that are cross-version (such as help and service discovery)
                    servicename + "_*",
                    new List<string>() { servicename + ".help"/*, servicename + ".scarfoldTS"*/ },
                    serviceMessageHandler.HandleMessage,
                    true)
                    );

                configurations.Add(new RequestConfiguration(//for handling routes that are cross-version (such as help and service discovery)
                    servicename + "_servicediscovery_*",
                    new List<string>() { "servicediscovery.#" },
                    serviceMessageHandler.HandleMessage,
                    true)
                    );

                configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                servicename + $".v{Resource.ProgramVersion.Major}",
                new List<string>() { servicename + $".v{Resource.ProgramVersion.Major}" + ".claims" },
                new MessageHandlers(Resource).HandleMessage)
                );

                configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                servicename + $".v{Resource.ProgramVersion.Major}",
                new List<string>() { servicename + $".v{Resource.ProgramVersion.Major}" + ".eventCallback" },
                new MessageHandlers(Resource).HandleMessage)
                );

                configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                servicename + $".v{Resource.ProgramVersion.Major}",
                Resource.Commands.Select(p => servicename + $".v{Resource.ProgramVersion.Major}" + "." + p.GetType().Name).ToList(),
                new MessageHandlers(Resource).HandleMessage)
                );

                var helpdto = new ServiceMessageHandlers(servicename, Resource).GetHelpDTO();
                EventClient.Instance.RaiseEvent("servicediscovery", new ReturnMessage(true, helpdto));
            }

            System.Console.WriteLine("Starting request server");
            using (var server = new RequestServer(configurations, diagnostics))
            {
                while (true)
                {
                    Thread.Sleep(1);
                }
            }
        }
    }
}
