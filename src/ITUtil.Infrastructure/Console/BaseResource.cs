﻿// <copyright file="IResource.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Infrastructure.Console
{
    using ITUtil.Common.Command.Interfaces;
    using System.Collections.Generic;
    using ITUtil.Infrastructure.Command;
    using ITUtil.Infrastructure.RabbitMQ.DTO;

    public abstract class BaseResource : IResource
    {
        protected ProgramVersion programVersion;
        protected string name;
        protected List<CommandRestRegistration> commands;
        protected List<IEventDefinitionBase> events;
        protected BaseResource(string name, ProgramVersion programVersion)
        {
            this.programVersion = programVersion;
            this.name = name.ToLower();
            commands = new List<CommandRestRegistration>();
            events = new List<IEventDefinitionBase>();
        }

        public string Name { get { return this.name; } }
        public List<CommandRestRegistration> Commands { get { return commands; } }

        public void AddCommand<TCommandModelIn, TCommandModelOut>(IFunction<TCommandModelIn, TCommandModelOut> command, ITUtilOperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest where TCommandModelOut : IDtoResponse
        {
            this.commands.Add(new FunctionRestRegistration<TCommandModelIn, TCommandModelOut>(command, operationType, name, description, claims));
        }
        public void AddCommand<TCommandModelOut>(IFunction<TCommandModelOut> command, ITUtilOperationType operationType, string name, string description, params Claim[] claims) where TCommandModelOut : IDtoResponse
        {
            this.commands.Add(new FunctionRestRegistration<TCommandModelOut>(command, operationType, name, description, claims));
        }

        public void AddCommand<TCommandModelIn>(IProcedure<TCommandModelIn> command, ITUtilOperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest
        {
            this.commands.Add(new ProcedureRestRegistration<TCommandModelIn>(command, operationType, name, description, claims));
        }
        public void AddCommand(IProcedure command, ITUtilOperationType operationType, string name, string description, params Claim[] claims)
        {
            this.commands.Add(new ProcedureRestRegistration(command, operationType, name, description, claims));
        }

        public List<IEventDefinitionBase> Events
        {
            get
            {
                var res = new List<IEventDefinitionBase>();
                res.AddRange(events);

                res.Add(new EventDefinition<EventCallback>("servicediscovery", $"This event is raised as a response to {this.name}.v{this.ProgramVersion.Major}.help"));

                foreach (var cmd in commands)
                {
                    res.Add(new EventDefinition<EventCallback>($"{this.name}.v{this.ProgramVersion.Major}.{cmd.GetType().Name}.succeeded", "This event is raised every time the command was successfully executed"));
                    res.Add(new EventDefinition<System.String>($"{this.name}.v{this.ProgramVersion.Major}.{cmd.GetType().Name}.failed", "This event is raised when a command failed on execution"));
                }

                return events;
            }
        }
        public ProgramVersion ProgramVersion { get { return this.programVersion; } }
    }
}
