﻿// <copyright file="IResource.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Infrastructure.Console
{
    using ITUtil.Common.Command.Interfaces;
    using ITUtil.Infrastructure.Command;
    using ITUtil.Infrastructure.Storage;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class CommandRestRegistration
    {
        public string Name { get; set; }
        public ICommand Command { get; set; }
        public ITUtilOperationType OperationType { get; set; }
        public string Description { get; set; }
        public List<Claim> Claims { get; set; }
        public Type ModelIn { get; set; }
        public Type ModelOut { get; set; }

        protected CommandRestRegistration(Type modelIn, Type modelOut, ICommand command, ITUtilOperationType operationType, string name, string description, params Claim[] claims)
        {
            switch (operationType)
            {
                case ITUtilOperationType.Event:
                    break;
                case ITUtilOperationType.Get:
                    if (!(name.Equals("get", StringComparison.OrdinalIgnoreCase) || name.Equals("getmany", StringComparison.OrdinalIgnoreCase) || name.Equals("getall", StringComparison.OrdinalIgnoreCase)))
                        throw new Exception("Get commands must be one of the following: 'get', 'getmany', 'getall'");
                    break;
                case ITUtilOperationType.Update:
                    if (!(name.Equals("update", StringComparison.OrdinalIgnoreCase) || name.Equals("updatelist", StringComparison.OrdinalIgnoreCase) || name.Equals("updatemany", StringComparison.OrdinalIgnoreCase) || name.Equals("updateall", StringComparison.OrdinalIgnoreCase)))
                        throw new Exception("Update commands must be one of the following: 'update', 'updatemany', 'updateall'");
                    break;
                case ITUtilOperationType.Insert:
                    if (!(name.Equals("insert", StringComparison.OrdinalIgnoreCase) || name.Equals("insertlist", StringComparison.OrdinalIgnoreCase)))
                        throw new Exception("Insert commands must be one of the following: 'insert', 'insertlist'");
                    break;
                case ITUtilOperationType.Delete:
                    if (!(name.Equals("delete", StringComparison.OrdinalIgnoreCase) || name.Equals("deletemany", StringComparison.OrdinalIgnoreCase)))
                        throw new Exception("Delete commands must be one of the following: 'delete', 'deletemany'");
                    break;
                case ITUtilOperationType.Execute:
                    break;
                default:
                    throw new Exception("RestOperationType not implemented");
            }

            if (name.StartsWith("get"))
            {
                if (modelIn == null || modelOut == null)
                {
                    throw new System.Exception("Get commands must have both models in and out");
                }

                //get* skal modtage en fielddefinition
                if (!modelIn.GetInterfaces().Any(x => x.GetType() == typeof(IFieldDefinition)))
                    throw new System.Exception("Get commands must implement IFieldDefinition in their TCommandModelIn");

                if (name.Equals("getmany") || name.Equals("getall"))
                {
                    if (!modelIn.GetInterfaces().Any(x => x.GetType() == typeof(IPagingDefinition)))
                        throw new System.Exception("GetMany and GetAll commands must implement IPagingDefinition in their TCommandModelIn");

                    if (!modelOut.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IPageResult<>)))
                        throw new System.Exception("GetMany and GetAll commands must implement IPagingDefinition in their TCommandModelOut");
                }
            }

            if (name.EndsWith("many"))
            {
                if (!modelIn.GetInterfaces().Any(x => x.GetType() == typeof(IFilterDefinition)))
                    throw new System.Exception("'Many' commands must implement IFilterDefinition in their TCommandModelIn");
            }

            if (name.EndsWith("list"))
            {
                if (!modelIn.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IInputList<>)))
                    throw new System.Exception("'List' commands must implement IInputList in their TCommandModelIn");
            }

            Command = command;
            OperationType = operationType;
            Name = name;
            Description = description;
            Claims = new List<Claim>(claims);
            ModelIn = modelIn;
            ModelOut = modelOut;
        }
    }
}
