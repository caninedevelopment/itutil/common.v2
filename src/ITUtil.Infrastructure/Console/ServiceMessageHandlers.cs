﻿using System;
using System.Threading.Tasks;
using ITUtil.Common;
using ITUtil.Infrastructure.RabbitMQ.DTO;
using ITUtil.Infrastructure.RabbitMQ.Event;
using ITUtil.Infrastructure.RabbitMQ.Message;

namespace ITUtil.Infrastructure.Console
{
    public class ServiceMessageHandlers
    {
        public ServiceMessageHandlers(string routenamespace, IResource instance)
        {
            this.RouteNamespace = routenamespace;
            this.Instance = instance;
        }
        private string RouteNamespace { get; }
        private IResource Instance { get;}

        //public ReturnMessage ScarfoldTS(string[] topicparts, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp)
        //{
        //    var routenamespace = topicparts[0];
        //    var operation = topicparts[1];

        //    if (operation == "scarfoldTS")
        //    {
        //        var generator = new TypeScriptFluent().WithConvertor<Guid>(c => "string");
        //        StringBuilder sb = new StringBuilder();

        //        //this.instance.GetType().Namespace vs. routenamespace!

        //        sb.Append("let baseUrl:string = \"https://xxx.xxxx.xxx/\";\r\n");
        //        sb.Append("export class " + this.instance.Name + " {\r\n");
        //        foreach (var command in instance.Commands)
        //        {
        //            Type type = command.GetType();
        //            var method = type.GetMethods().Where(p => p.Name == "Execute").FirstOrDefault();
        //            var outputType = method.ReturnType;
        //            var inputType = method.GetParameters()[0].ParameterType;

        //            if (outputType != typeof(object))
        //                generator.ModelBuilder.Add(outputType);
        //            if (inputType != typeof(object))
        //                generator.ModelBuilder.Add(inputType);

        //            var outputtype = outputType.Namespace + "." + outputType.Name;
        //            if (outputtype == "System.Object")
        //            {
        //                outputtype = "void";
        //            }
        //            else
        //            {
        //                outputtype += " | undefined";
        //            }

        //            var intputtype = inputType.Namespace + "." + inputType.Name;
        //            if (intputtype == "System.Object")
        //            {
        //                intputtype = "void";
        //            }

        //            sb.Append("\t/*" + command.Description + "*/\r\n");

        //            if (command.Claims.Any())
        //            {
        //                if (intputtype == "void")
        //                    sb.Append("\tstatic async " + type.Name + "(token : string) : Promise<" + outputtype + ">\r\n");
        //                else
        //                    sb.Append("\tstatic async " + type.Name + "(input: " + inputType.Namespace + "." + inputType.Name + ", token : string) : Promise<" + outputtype + ">\r\n");
        //            }
        //            else
        //            {
        //                if (intputtype == "void")
        //                    sb.Append("\tstatic async " + type.Name + "() : Promise<" + outputtype + ">\r\n");
        //                else
        //                    sb.Append("\tstatic async " + type.Name + "(input: " + inputType.Namespace + "." + inputType.Name + ") : Promise<" + outputtype + ">\r\n");
        //            }

        //            sb.Append("\t{\r\n");
        //            sb.Append("\t \tvar response = await fetch(baseUrl + '" + instance.Name.ToLower() + "/v" + instance.ProgramVersion.major + "/" + type.Name + "',\r\n");
        //            sb.Append("\t \t{\r\n");
        //            sb.Append("\t \t  method: 'post',\r\n");

        //            if (command.Claims.Any())
        //                sb.Append("\t \t  headers: new Headers({ TokenId: token }),\r\n");

        //            if (intputtype != "void")
        //                sb.Append("\t \t  body: JSON.stringify(input),\r\n");

        //            sb.Append("\t \t});\r\n");
        //            //sb.Append("\t \tvar data = await response.json();\r\n");

        //            if (outputtype != "void")
        //            {//TODO: UPDATE!!
        //                sb.Append("\t \treturn await response.json();\r\n");

        //                //sb.Append("\t \tif (data.success)\r\n");
        //                //sb.Append("\t \t{ \r\n");
        //                //sb.Append("\t \t  return JSON.parse(data.data);\r\n");
        //                //sb.Append("\t \t}\r\n");
        //                //sb.Append("\t \telse\r\n");
        //                //sb.Append("\t \t{\r\n");
        //                //sb.Append("\t \t  return undefined;\r\n");
        //                //sb.Append("\t \t}\r\n");
        //            }
        //            else
        //            {
        //                sb.Append("\t \tawait response.json();\r\n");
        //            }
        //            sb.Append("\t}\r\n");
        //        }
        //        sb.Append("}");

        //        var typescript = generator.Generate();
        //        sb.Append("\t" + typescript);

        //        var messagewrapper = new ReturnMessage(true, sb.ToString(), LocalizedString.OK);
        //        return messagewrapper;
        //    }
        //    else
        //    {
        //        return new ReturnMessage(true, null, LocalizedString.UnknownOperation);
        //    }
        //}

        public async Task<ReturnMessage> HandleMessage(string[] topicparts, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatingIP)
        {
            //var routenamespace = topicparts[0];
            var operation = topicparts[1];
            if (operation == "help")
            {
                var messagewrapper = new ReturnMessage(true, GetHelpDTO(), LocalizedString.OK);
                EventClient.Instance.RaiseEvent("servicediscovery", messagewrapper);
                return messagewrapper;
            }
            //if (operation == "scarfoldTS")
            //{//TODO: Events?
            //    return ScarfoldTS(topicparts, messageAsJson, tokenInfo, messageTimestamp);
            //}
            else
            {
                return new ReturnMessage(true, null, LocalizedString.UnknownOperation);
            }
        }

        public ServiceDescriptors GetHelpDTO()
        {
            //TODO: Opdater ServiceDescriptors til eget format (minus swagger her!)
            ServiceDescriptors serviceDescriptor = new ServiceDescriptors();
            serviceDescriptor.Services.Add(new ServiceDescriptor()
            {
                version = Instance.ProgramVersion,
                operationDescriptions = OperationDescription.Convert(Instance),
                serviceName = RouteNamespace,
            });

            serviceDescriptor.Services.Add(new ServiceDescriptor(
                    Instance.ProgramVersion,
                    new OperationDescription() { Operation = "claims", Description = "returns claims defined by the microservice" }));

            return serviceDescriptor;
        }
    }
}
