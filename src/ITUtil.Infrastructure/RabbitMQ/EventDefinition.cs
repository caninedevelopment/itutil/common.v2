﻿// <copyright file="EventDefinition.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Infrastructure.Command
{
    public interface IEventDefinitionBase
    {
        string Route { get; }
        string Description { get; }
    }

    public interface IEventDefinition<DtoType> : IEventDefinitionBase where DtoType : class
    {
        DtoType Dto();
    }
    public class EventDefinition<DtoType> : IEventDefinition<DtoType> where DtoType : class
    {
        protected string description;
        protected string route;

        public EventDefinition(string route, string description)
        {
            this.route = route;
            this.description = description;
        }

        public string Description { get { return this.description; } }
        public string Route { get { return this.route; } }
        public DtoType Dto() { return default; }
    }
}
