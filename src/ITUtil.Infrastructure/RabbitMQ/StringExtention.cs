﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITUtil.Infrastructure.RabbitMQ.Common
{
    internal static class StringExtention
    {
        public static string CamelCase(this string str)
        {
            return string.IsNullOrEmpty(str) || str.Length < 2
                   ? str
                   : Char.ToLowerInvariant(str[0]) + str.Substring(1);
        }
    }
}
