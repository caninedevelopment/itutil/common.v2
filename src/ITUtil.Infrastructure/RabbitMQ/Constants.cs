﻿using ITUtil.Infrastructure.RabbitMQ.Message;

namespace ITUtil.Infrastructure.RabbitMQ
{
    public static class Constants
    {
        public const string EventExchangeName = "ms.events";
        public const string RequestExchangeName = "ms.request";
        public const string QueuePrefix = "ms.queue.";
        public delegate void EventHandler(string[] topicparts, ReturnMessage data);
    }
}
