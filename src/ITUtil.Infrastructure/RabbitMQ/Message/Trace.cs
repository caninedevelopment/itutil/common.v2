﻿namespace ITUtil.Infrastructure.RabbitMQ.Message
{
    public class Trace
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Trace"/> class.
        /// Default constructor used by the deserialiser
        /// </summary>
        public Trace()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Trace"/> class.
        /// </summary>
        /// <param name="IP">the IP adress of the machine running the program</param>
        /// <param name="service">Name of the microservice that created this trace</param>
        /// <param name="topic">A brief description of the trace, would normally be the message-route</param>
        public Trace(string topic, string requestData, string internalTrace)
        {
            this.Topic = topic;
            this.RequestData = requestData;
            this.InternalTrace = internalTrace;
        }
        /// <summary>
        /// Gets or sets the requestData
        /// </summary>
        public string RequestData { get; set; }

        /// <summary>
        /// Gets or sets the topic
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Gets or sets the traceinformation
        /// </summary>
        public string InternalTrace { get; set; }
    }
}
