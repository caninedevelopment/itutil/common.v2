﻿// <copyright file="MessageQueueConfiguration.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using System.Collections.Generic;

namespace ITUtil.Infrastructure.RabbitMQ.Message
{
    /// <summary>
    /// An abstract class for messagequeueconfiguration - please us a concrete class such as EventConfiguration or RequestConfiguration
    /// </summary>
    public abstract class MessageQueueConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageQueueConfiguration"/> class.
        /// </summary>
        /// <param name="exchangeName">Name of the exchange from where messages are received</param>
        /// <param name="queueName">Name of the queue onto where messages are to be placed</param>
        /// <param name="routingKey">Define the route that describes the messages to be placed in the queue</param>
        protected MessageQueueConfiguration(string exchangeName, string queueName, List<string> routingKeys)
        {
            this.ExchangeName = exchangeName;
            this.QueueName = queueName;
            this.RoutingKeys = routingKeys;
        }

        /// <summary>
        /// Gets or sets ExchangeName
        /// </summary>
        public string ExchangeName { get; set; }

        /// <summary>
        /// Gets or sets QueueName
        /// </summary>
        public string QueueName { get; set; }

        /// <summary>
        /// Gets or sets RoutingKey
        /// </summary>
        public List<string> RoutingKeys { get; set; }
    }
}
