﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ITUtil.Infrastructure.RabbitMQ.Message
{
    using Command.Interfaces;
    using ITUtil.Infrastructure.Command;
    using ITUtil.Infrastructure.Console;
    using ITUtil.Infrastructure.RabbitMQ.Common;

    public class OperationDescription
    {
        public static List<OperationDescription> Convert(IResource instance)
        {
            List<OperationDescription> res = new List<OperationDescription>();
            foreach (var instanceDescription in instance.Commands)
            {
                res.Add(new OperationDescription(instanceDescription, instance));
            }
            foreach (var e in instance.Events)
            {
                res.Add(new OperationDescription(e/*, instance*/));
            }

            return res;
        }
        public OperationDescription()
        {
        }

        public OperationDescription(IEventDefinitionBase e/*, IResource instance*/)
        {
            //TODO!!!
            var type = e.GetType();
            var method = Array.Find(type.GetMethods(), p => p.Name == "Dto");
            var outputType = method.ReturnType;
            this.Operation = e.Route;
            this.Type = ITUtilOperationType.Event;
            this.Description = e.Description;
            this.DataOutputExample = ConvertTypeToOpenApi(outputType, new Dictionary<Type, int>());
        }

        public OperationDescription(CommandRestRegistration command, IResource instance)
        {
            this.Operation = command.Name;
            this.Description = command.Description;
            this.Type = command.OperationType;

            //var method = type.GetMethods().Where(p => p.Name == "Execute").FirstOrDefault(); // type.GetMethod("Execute");
            var outputType = command.ModelOut;// method.ReturnType;

            if (command.ModelIn != null)
            {
                this.DataInputExample = ConvertTypeToOpenApi(command.ModelIn, new Dictionary<Type, int>());
            }
            else
            {
                this.DataInputExample = null;
            }

            if (outputType == null)
                this.DataOutputExample = null;
            else
                this.DataOutputExample = ConvertTypeToOpenApi(outputType, new Dictionary<Type, int>());

            this.RequiredClaims = new List<Claim>();
            foreach (var c in command.Claims)
            {
                this.RequiredClaims.Add(new Claim()
                {
                    key = instance.Name.ToLower() + "." + c.key.ToLower(),
                    dataContext = c.dataContext,
                    description = c.description
                });
            }
        }

        private ApiSchema ConvertTypeToOpenApi(Type datatype, Dictionary<Type, int> seentypes, ApiSchema.DataTypeEnum type = ApiSchema.DataTypeEnum.Object)
        {
            bool excludeHeaderPropertiesFromDocumentation = datatype.GetInterfaces().Any(x => x.GetType() == typeof(IITUtilHeaderInfo));
            var excludedProperties = typeof(IITUtilHeaderInfo).GetProperties();

            ApiSchema schema = new ApiSchema();
            if (datatype != null)
            {
                schema.DataType = type;
                foreach (var property in datatype.GetProperties())
                {
                    if (property.CanWrite && property.CanRead)
                    {
                        if (excludeHeaderPropertiesFromDocumentation && excludedProperties.Any(x => x.Name == property.Name))
                            break;

                        var typeInfo = property.PropertyType.GetTypeInfo();
                        schema.IsNullable = typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>);

                        ApiSchema.DataTypeEnum nexttype;
                        if (property.PropertyType.IsGenericType && (
                            property.PropertyType.GetGenericTypeDefinition() == typeof(List<>) ||
                            property.PropertyType.GetGenericTypeDefinition() == typeof(IList<>) ||
                            property.PropertyType.GetGenericTypeDefinition() == typeof(IEnumerable<>) ||
                            property.PropertyType.GetGenericTypeDefinition() == typeof(Enumerable) ||
                            property.PropertyType.GetGenericTypeDefinition() == typeof(Array)))
                        {
                            nexttype = ApiSchema.DataTypeEnum.Array;
                        }
                        if (property.PropertyType.GetType() == typeof(Enum))
                        {
                            nexttype = ApiSchema.DataTypeEnum.Enum;
                        }
                        else
                        {
                            nexttype = ApiSchema.DataTypeEnum.Object;
                        }

                        if (type == ApiSchema.DataTypeEnum.Array)
                        {
                            schema.Items = ConvertTypeToOpenApi(property.PropertyType, seentypes, nexttype);
                        }
                        else
                        {
                            if (IsSimple(property.PropertyType))
                            {
                                schema.Properties.Add(
                                    new ApiSchema()
                                    {
                                        Name = property.Name.CamelCase(),
                                        DataType = GetPropertyJSType(property),
                                        Description = "TODO",
                                        ValidValues = property.PropertyType.IsEnum ? Enum.GetNames(property.PropertyType).ToList() : null
                                    });
                            }
                            else if (!seentypes.ContainsKey(property.PropertyType) || (seentypes.ContainsKey(property.PropertyType) && seentypes[property.PropertyType] < 10))
                            { // TODO: hvordan skal vi håndtere cirkulær referencer?
                              //array
                                if (seentypes.ContainsKey(property.PropertyType))
                                    seentypes[property.PropertyType]++;
                                else
                                    seentypes.Add(property.PropertyType, 1);

                                schema.Properties.Add(
                                    ConvertTypeToOpenApi(property.PropertyType, seentypes, nexttype)
                                    );
                            }
                        }
                    }
                }

                return schema;
            }
            else
            {
                return null;
            }
        }

        private bool IsSimple(Type type)
        {
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return IsSimple(typeInfo.GetGenericArguments()[0]);
            }
            return typeInfo.IsPrimitive
              || typeInfo.IsEnum
              || type.Equals(typeof(string))
              || type.Equals(typeof(Guid))
              || type.Equals(typeof(decimal));
        }
        private ApiSchema.DataTypeEnum GetPropertyJSType(PropertyInfo pi)
        {
            var typeInfo = pi.PropertyType.GetTypeInfo();
            bool isNullable = typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>);

            Type propertyType = pi.PropertyType;
            if (isNullable)
            {
                propertyType = Nullable.GetUnderlyingType(pi.PropertyType);
            }
            string variablename = propertyType.Name.ToLower();

            switch (variablename)
            {
                case "guid":
                    return ApiSchema.DataTypeEnum.Guid;
                case "string":
                    return ApiSchema.DataTypeEnum.String;
                case "double":
                case "decimal":
                    return ApiSchema.DataTypeEnum.Decimal;
                case "int":
                case "int16":
                case "int32":
                    return ApiSchema.DataTypeEnum.Number;
                case "uint16":
                case "uint32":
                    return ApiSchema.DataTypeEnum.UnsignedNumber;
                case "long":
                case "int64":
                    return ApiSchema.DataTypeEnum.BigInt;
                case "uint64":
                    return ApiSchema.DataTypeEnum.UnsignedBigInt;
                case "bool":
                case "boolean":
                    return ApiSchema.DataTypeEnum.Boolean;
                default:
                    if (propertyType.IsEnum)
                    {
                        return ApiSchema.DataTypeEnum.Enum;
                    }
                    else
                    {
                        throw new Exception("Unknown datatype " + pi.PropertyType.Name + " for " + pi.Name);
                    }
            }
        }
        public string Operation { get; set; } //ex. "Insert", "Update", "Delete", "Get" etc.
        public string Description { get; set; } //developer description
        public ApiSchema DataInputExample { get; set; } //json description
        public ApiSchema DataOutputExample { get; set; } //json description
        public List<Claim> RequiredClaims { get; set; }

        public ITUtilOperationType Type { get; set; }
    }

    public class ApiSchema
    {
        public enum DataTypeEnum
        {
            Array, //--> Items nedenfor beskriver hvilken type objekter der er i array
            Object, //--> Properties nedenfor beskriver de properties der er på objektet
            Enum, //--> ValidValues nedenfor beskriver de værdier enum kan have
            //nedenstående er simple typer, hvor validvalues, items og properties alle er null
            Guid,
            String,
            UnsignedNumber,
            UnsignedBigInt,
            Decimal,
            Number,
            BigInt,
            Boolean
        }
        public DataTypeEnum DataType { get; set; }
        public bool IsNullable { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> ValidValues { get; set; }
        public List<ApiSchema> Properties { get; set; }

        public ApiSchema Items { get; set; }
    }

    //TODO: Opdater gateway til v 4.0.0.0 så denne håndtere ovenstående struktur / konvertere den til openapi
}
