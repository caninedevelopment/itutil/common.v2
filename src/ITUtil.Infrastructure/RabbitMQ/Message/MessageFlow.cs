﻿// <copyright file="MessageFlow.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Infrastructure.RabbitMQ.Message
{
    using System;
    using EventHandler = Constants.EventHandler;
    using global::RabbitMQ.Client.Events;
    using ITUtil.Infrastructure.RabbitMQ.DTO;
    using ITUtil.Infrastructure.RabbitMQ.Common;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for handling the generic part of the messageflow, unwrapping the wrapper and handling diagnostics
    /// </summary>
    public static class MessageFlow
    {
        /// <summary>
        /// A delegate description for handling request messages
        /// </summary>
        /// <param name="topicparts">a list of route parts (ex: [user, create])</param>
        /// <param name="wrapper">The messagewrapper object</param>
        /// <returns>the returning object serialised as json and converted to utf8 byte array</returns>
        public delegate Task<ReturnMessage> MessageHandler(string[] topicparts, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatedByIp);

        /// <summary>
        /// Handles the basic messageflow for events
        /// </summary>
        /// <param name="servicename">name of the calling service</param>
        /// <param name="ea">the rabbitMQ delivery event</param>
        /// <param name="handler">the message handler for handling specifik logic</param>
        public static void HandleEvent(/*string servicename, */BasicDeliverEventArgs ea, EventHandler handler)
        {
            string[] topicparts = ea.RoutingKey.Split('.');
            if (ea.Body.Length != 0)
            {
                var wrapper = Newtonsoft.Json.JsonConvert.DeserializeObject<ReturnMessage>(System.Text.Encoding.UTF8.GetString(ea.Body.ToArray()));
                handler?.Invoke(topicparts, wrapper);
            }
        }
    }
}