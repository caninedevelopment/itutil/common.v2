﻿using ITUtil.Common;

namespace ITUtil.Infrastructure.RabbitMQ.Message
{
    public class ReturnMessage
    {
        public ReturnMessage()
        {
        }

        public ReturnMessage(string json)
        {
            this.Data = System.Text.Encoding.UTF8.GetBytes(json);
            this.Message = null;
            this.Success = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReturnMessage"/> class.
        /// </summary>
        /// <param name="data">the return object as json (utf8 byte array)</param>
        public ReturnMessage(byte[] data)
        {
            this.Data = data;
            this.Message = null;
            this.Success = true;
        }

        public ReturnMessage(object data)
        {
            string dataAsJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            this.Data = System.Text.Encoding.UTF8.GetBytes(dataAsJson);
            this.Message = null;
            this.Success = true;
        }
        public ReturnMessage(bool success, object data, LocalizedString message = null)
        {
            if (data is string s)
            {
                this.Data = System.Text.Encoding.UTF8.GetBytes(s);
            }
            else
            {
                string dataAsJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                this.Data = System.Text.Encoding.UTF8.GetBytes(dataAsJson);
            }
            this.Message = message;
            this.Success = success;
        }

        /// <summary>
        /// Gets or sets Message
        /// </summary>
        public LocalizedString Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets Success
        /// </summary>
        public bool Success { get; set; }

        public string Diagnostics { get; set; }

        /// <summary>
        /// Gets or sets data (json serialized object, which have been converted to utf8 byte array)
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets responseToClientId. Only used for FAF calls... which clientId to return the message to
        /// </summary>
        public string ResponseToClientId { get; set; } //TODO: Overvej om de skal sendes i header tilbage også (i.e. håndtering af dette både server og clientside)
        /// <summary>
        /// Gets or sets responseToClientId. Only used for FAF calls... which clientId to return the message to
        /// </summary>
        public string ResponseToMessageId { get; set; } //TODO: Overvej om de skal sendes i header tilbage også (i.e. håndtering af dette både server og clientside)
    }
}
