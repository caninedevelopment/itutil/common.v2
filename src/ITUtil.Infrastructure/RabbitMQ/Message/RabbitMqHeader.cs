﻿using ITUtil.Infrastructure.RabbitMQ.DTO;
using System;

namespace ITUtil.Infrastructure.RabbitMQ.Message
{
    public class RabbitMqHeader
    {
        public RabbitMqHeader() { }
        public RabbitMqHeader(TokenInfo tokenInfo, DateTime messageIssueDate, bool isDirectLink, bool includeDiagnostics, string clientId, string messageId, string ip = "")
        {
            this.TokenInfo = tokenInfo;
            this.MessageIssueDate = messageIssueDate;
            this.IsDirectLink = isDirectLink;
            this.ClientId = clientId;
            this.MessageId = messageId;
            this.Ip = ip;
            this.IncludeDiagnostics = includeDiagnostics;
        }
        public TokenInfo TokenInfo { get; set; }
        public DateTime MessageIssueDate { get; set; }
        public bool IsDirectLink { get; set; }
        public bool IncludeDiagnostics { get; set; }
        public string ClientId { get; set; }
        public string MessageId { get; set; }
        public string Ip { get; set; }
    }
}
