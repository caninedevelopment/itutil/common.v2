﻿using System.Collections.Generic;

namespace ITUtil.Infrastructure.RabbitMQ.DTO
{
    /// <summary>
    /// Use for communicating claim requirements of a specific service.
    /// </summary>
    public class ClaimDefinitions
    {
        public ClaimDefinitions(List<Command.Claim> definitions)
        {
            this.Definitions = definitions;
        }

        /// <summary>
        /// Gets or sets Definitions
        /// </summary>
        public List<Command.Claim> Definitions { get; set; }
    }
}
