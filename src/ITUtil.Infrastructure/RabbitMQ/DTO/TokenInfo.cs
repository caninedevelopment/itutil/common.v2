﻿using System;
using System.Collections.Generic;

namespace ITUtil.Infrastructure.RabbitMQ.DTO
{
    /// <summary>
    /// TokenInfo description
    /// </summary>
    public class TokenInfo
    {
        public class Claim
        {
            /// <summary>
            /// Gets or sets Key
            /// </summary>
            public string key { get; set; }

            /// <summary>
            /// Gets or sets Value
            /// </summary>
            public string value { get; set; }

            /// <summary>
            /// Gets or sets Scope
            /// </summary>
            public Command.Claim.DataContextEnum scope { get; set; }
        }

        /// <summary>
        /// Gets or sets Tokenid
        /// </summary>
        public Guid tokenId { get; set; }

        /// <summary>
        /// Gets or sets Userid
        /// </summary>
        public Guid userId { get; set; }

        /// <summary>
        /// Gets or sets ValidUntil
        /// </summary>
        public DateTime validUntil { get; set; }

        /// <summary>
        /// Gets or sets Claims
        /// </summary>
        public List<Claim> claims { get; set; }

        public bool IsValidToken()
        {
            return this.validUntil > DateTime.Now;
        }
    }
}
