﻿namespace ITUtil.Infrastructure.RabbitMQ.DTO
{
    using System.Collections.Generic;
    using ITUtil.Common.Command;
    using ITUtil.Infrastructure.Console;
    using ITUtil.Infrastructure.RabbitMQ.Message;

    public class ServiceDescriptors
    {
        public List<ServiceDescriptor> Services { get; set; } = new List<ServiceDescriptor>();
    }

    public class ServiceDescriptor
    {
        public ServiceDescriptor()
        {
            this.operationDescriptions = new List<OperationDescription>();
        }

        public ServiceDescriptor(ProgramVersion version, OperationDescription operationDescription)
        {
            this.version = version;
            this.operationDescriptions = new List<OperationDescription>() { operationDescription };
        }

        public ServiceDescriptor(OperationDescription operationDescription)
        {
            this.operationDescriptions = new List<OperationDescription>() { operationDescription };
        }
        public string serviceName { get; set; }

        public ProgramVersion version { get; set; }

        public List<OperationDescription> operationDescriptions { get; set; }
    }
}
