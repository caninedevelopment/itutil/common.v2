﻿using System;

namespace ITUtil.Infrastructure.RabbitMQ.DTO
{
    public class InvalidateUserData
    {
        /// <summary>
        /// Gets or sets Userids
        /// </summary>
        public Guid[] userIds { get; set; }

        /// <summary>
        /// Gets or sets ValidUntil
        /// </summary>
        public DateTime validUntil { get; set; }
    }
}
