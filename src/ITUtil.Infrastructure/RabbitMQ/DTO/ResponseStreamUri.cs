﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITUtil.Infrastructure.RabbitMQ.DTO
{
    public class ResponseStreamUri
    {
        public string Uri { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
