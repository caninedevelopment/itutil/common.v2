﻿using System;

namespace ITUtil.Infrastructure.RabbitMQ.DTO
{
    public class EventCallback
    {
        public Guid id { get; set; }
        public string route { get; set; }
    }
}
