﻿// <copyright file="EventConfiguration.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using ITUtil.Infrastructure.RabbitMQ.Message;
using System.Collections.Generic;

namespace ITUtil.Infrastructure.RabbitMQ.Event
{
    /// <summary>
    /// Configuration for event messages
    /// </summary>
    public class EventConfiguration : MessageQueueConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventConfiguration"/> class.
        /// </summary>
        /// <param name="queueName">Name of the queue that the messages are to be placed in</param>
        /// <param name="routingKey">Name of the routing which messages are to be placed on the queue</param>
        /// <param name="handler">The message handler</param>
        public EventConfiguration(string queueName, List<string> routingKeys, Constants.EventHandler handler)
            : base(Constants.EventExchangeName, queueName, routingKeys)
        {
            this.Handler = handler;
        }

        /// <summary>
        /// Gets or sets the handler, for handling events
        /// </summary>
        public Constants.EventHandler Handler { get; set; }
    }
}
