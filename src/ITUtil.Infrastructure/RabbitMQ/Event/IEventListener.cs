﻿
using static ITUtil.Infrastructure.RabbitMQ.Constants;

namespace ITUtil.Infrastructure.RabbitMQ.Common.Event
{
    public interface IEventListener
    {
        /// <summary>
        /// Should contain a unique service name
        /// </summary>
        string ServiceName { get; }

        /// <summary>
        /// List of operations that can be performed by this service
        /// </summary>
        EventHandler Handler { get; }
    }
}
