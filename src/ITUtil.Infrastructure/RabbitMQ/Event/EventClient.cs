﻿// <copyright file="EventClient.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Infrastructure.RabbitMQ.Event
{
    using System;
    using global::RabbitMQ.Client;
    using global::RabbitMQ.Client.Events;
    using ITUtil.Infrastructure.Config;
    using ITUtil.Infrastructure.RabbitMQ.Message;

    /// <summary>
    /// A client for sending events to the message queue
    /// </summary>
    public class EventClient
    {
        private static EventClient instance = null;

        private readonly IConnection connection;

        private readonly IModel channel;

        private readonly EventingBasicConsumer consumer;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventClient"/> class.
        /// </summary>
        private EventClient()
        {
            var config = GlobalRessources.GetConfig().GetSetting<RabbitMQSettings>();
            var serverparts = config.Server.Split(':');
            var factory = new ConnectionFactory()
            {
                Port = serverparts.Length > 1 ? int.Parse(serverparts[1]) : 5672,
                HostName = serverparts[0],
                UserName = config.Username,
                Password = config.Password
            };
            factory.AutomaticRecoveryEnabled = true;
            factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);
            this.connection = factory.CreateConnection();
            this.channel = this.connection.CreateModel();
            this.channel.ExchangeDeclare(exchange: ITUtil.Infrastructure.RabbitMQ.Constants.EventExchangeName, type: "topic");
            this.consumer = new EventingBasicConsumer(this.channel);
        }

        ~EventClient()
        {
            this.connection.Close();
        }

        /// <summary>
        /// Gets the singleton instance of the client
        /// </summary>
        public static EventClient Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventClient();
                }

                return instance;
            }
        }

        /// <summary>
        /// Places an event on the message queue
        /// </summary>
        /// <param name="route">The topic/route of the event (ex. user.created)</param>
        /// <param name="eventdata">The event details</param>
        public void RaiseEvent(string route, ReturnMessage eventdata)
        {
            byte[] bsonMessage = System.Text.Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(eventdata));

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;
            channel.ExchangeDeclare(exchange: "ms.events", type: "topic");
            channel.BasicPublish(
                exchange: "ms.events",
                routingKey: route,
                basicProperties: properties,
                body: bsonMessage);
        }
    }
}