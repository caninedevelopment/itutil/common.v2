﻿using ITUtil.Infrastructure.RabbitMQ.DTO;

namespace ITUtil.Infrastructure.Command.Interfaces
{
    public interface IITUtilHeaderInfo
    {
        string InitiatedByIP { get; set; }
        TokenInfo HeaderInformation { get; set; }
    }
}
