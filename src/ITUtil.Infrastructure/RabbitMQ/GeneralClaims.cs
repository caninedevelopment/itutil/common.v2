﻿// <copyright file="GeneralClaims.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
using ITUtil.Common;
using System;

namespace ITUtil.Infrastructure.Command
{
    public partial class Claim
    {
        /// <summary>
        /// User is administrator, i.e. can see all orders.
        /// </summary>
        public static Claim IsAdmininistrator = new Claim()
        {
            key = "IsAdmininistrator",
            dataContext = DataContextEnum.organisationClaims,
            description = new[]
            {
                new LocalizedString(new Guid("15c1e210-32a7-4f14-ab10-f8b11bbda809"),"User is system administrator, i.e. can setup the service"),
            },
        };
        public static Claim IsOperator = new Claim()
        {
            key = "IsOperator",
            dataContext = DataContextEnum.organisationClaims,
            description = new[]
            {
                new LocalizedString(new Guid("88b08ac7-e826-4eb1-99f5-6915d810cc8b"),"User is an operator, i.e. has full access to administate the services data"),
            },
        };
        public static Claim IsConsumer = new Claim()
        {
            key = "IsConsumer",
            dataContext = DataContextEnum.organisationClaims,
            description = new[]
            {
                new LocalizedString(new Guid("3f0bc871-79d7-4dbe-ac89-99022c97627e"),"User is a data consumer, i.e. has access read data"),
            },
        };
    }
}