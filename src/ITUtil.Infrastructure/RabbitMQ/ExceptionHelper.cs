﻿using System;
using System.Text;

namespace ITUtil.Infrastructure.RabbitMQ
{
    internal static class ExceptionHelper
    {
        /// <summary>
        /// Gets exception as string
        /// </summary>
        /// <param name="ex">Exception to tranform into text</param>
        /// <returns>exception string</returns>
        public static string GetExceptionAsReportText(this Exception ex)
        {
            StringBuilder exception = new StringBuilder();
            StringBuilder trace = new StringBuilder();
            Exception e = ex;
            while (e != null)
            {
                exception.AppendLine(e.Message);
                trace.AppendLine(e.StackTrace);
                e = e.InnerException;
            }

            string reporttxt = string.Format("Exception: {0} \r\n Trace: {1}", exception, trace);
            return reporttxt;
        }
    }
}
