// <copyright file="Claim.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
using ITUtil.Common;

namespace ITUtil.Infrastructure.Command
{
    public partial class Claim
    {
        public enum DataContextEnum
        {
            /// <summary>
            /// Claims that the user have given to the organisation, such as "can edit my profile", "allowed to send me a newsletter" etc.
            /// </summary>
            userClaims,
            /// <summary>
            /// Claims that the organisation have given to a user, suchs as "is database administrator"
            /// </summary>
            organisationClaims,
        }

        public string key { get; set; }

        public LocalizedString[] description { get; set; }

        public DataContextEnum dataContext { get; set; }
    }
}
