﻿
namespace ITUtil.Infrastructure.RabbitMQ
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ITUtil.Common;
    using ITUtil.Common.Command;
    using ITUtil.Common.Command.Interfaces;
    using ITUtil.Infrastructure.Command.Interfaces;
    using ITUtil.Infrastructure.Console;
    using ITUtil.Infrastructure.RabbitMQ.Common;
    using ITUtil.Infrastructure.RabbitMQ.DTO;
    using ITUtil.Infrastructure.RabbitMQ.Event;
    using ITUtil.Infrastructure.RabbitMQ.Message;

    public class MessageHandlers
    {
        public MessageHandlers(IResource instance)
        {
            this.Instance = instance;
        }
        private IResource Instance { get; }

        public void RaiseEvent(string eventname, ReturnMessage eventdata)
        {
            EventClient.Instance.RaiseEvent(eventname, eventdata);
        }
        public async Task<ReturnMessage> HandleMessage(string[] topicparts, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatedbyIp)
        {
            var routenamespace = topicparts[0];

            var operation = topicparts[1];
            var command = Instance.Commands.Find(p => string.Equals(p.GetType().Name, operation, StringComparison.OrdinalIgnoreCase));

            if (!CheckAccess(tokenInfo, messageTimestamp, command.Claims, Instance))
            {
                string expecting = string.Join(" or ", command.Claims.Select(p => p.key));
                return new ReturnMessage(false, null, LocalizedString.MissingCredentials /* + " : " + expecting*/);
            }
            else
            {
                var baseDir = System.IO.Directory.GetCurrentDirectory() + "/events/";

                if (operation == "claims")
                {
                    List<Command.Claim> definitions = new List<Command.Claim>();

                    foreach (var cmd in Instance.Commands) //instance.OperationDescription)
                    {
                        foreach (var claimDefinition in cmd.Claims)
                        {
                            if (!definitions.Any(p => p.key == claimDefinition.key))
                            {
                                definitions.Add(claimDefinition);
                            }
                        }
                    }
                    ClaimDefinitions claims = new ClaimDefinitions(definitions);

                    return new ReturnMessage(true, new ClaimResult() { Claims = claims, Version = Instance.ProgramVersion }, LocalizedString.OK);
                }

                if (operation == "eventCallback")
                {
                    var request = Newtonsoft.Json.JsonConvert.DeserializeObject<EventCallback>(messageAsJson);
                    var filename = baseDir + request.id.ToString() + ".event";
                    if (System.IO.File.Exists(filename))
                    {
                        var filedata = System.IO.File.ReadAllText(filename);
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<ReturnMessage>(filedata);
                    }
                    else
                    {
                        return new ReturnMessage(false, null, new LocalizedString(new Guid("32a94896-6d5d-4ef1-b88b-63875ceddfa0"), "Event data does not exists"));
                    }
                }

                if (command != null)
                {
                    var TTL = DateTime.Now.AddHours(-1);
                    Guid EventId = Guid.NewGuid();
                    string eventRoute = $"{topicparts[0]}.v{Instance.ProgramVersion.Major}.{topicparts[1]}";
                    string callbackRoute = $"{topicparts[0]}.v{Instance.ProgramVersion.Major}.eventCallback";

                    var t = command.GetType();//redundant med linje 30, måske det skal slås op en gang for alle og gemmes i et dic?
                    var method = Array.Find(t.GetMethods(), p => p.Name == "Execute" && p.GetParameters().Length > 1);// t.GetMethod("Execute");

                    //bool excludeHeaderPropertiesFromDocumentation = datatype.GetInterfaces().Any(x => x.GetType() == typeof(IITUtilHeaderInfo));
                    //var excludedProperties = typeof(IITUtilHeaderInfo).GetProperties();

                    var parameters = method.GetParameters();

                    ReturnMessage result = null;

                    var handlemessageTask = new Task(() =>
                    {
                        bool isFunctionCmd = t.GetInterfaces()
                            .Any(x =>
                            x.IsGenericType &&
                            (
                            x.GetGenericTypeDefinition() == typeof(IFunction<,>) ||
                            x.GetGenericTypeDefinition() == typeof(IFunction<>)
                            ));
                        object[] input = new object[] { };
                        if (parameters.Length > 0)
                        {
                            var cmd_input = method.GetParameters()[0];
                            var cmd_input_instance = Activator.CreateInstance(cmd_input.ParameterType);
                            if (!string.IsNullOrEmpty(messageAsJson))
                            {
                                Newtonsoft.Json.JsonConvert.PopulateObject(messageAsJson, cmd_input_instance);
                            }

                            if (cmd_input_instance is IITUtilHeaderInfo)
                            {
                                IITUtilHeaderInfo cmd_input_headerInfo = (cmd_input_instance as IITUtilHeaderInfo);
                                if(cmd_input_headerInfo != null) {
                                    cmd_input_headerInfo.InitiatedByIP = initiatedbyIp;
                                    cmd_input_headerInfo.HeaderInformation = tokenInfo;
                                }
                            }

                            if (cmd_input_instance is IDtoRequest)
                            {
                                (cmd_input_instance as IDtoRequest)?.Validate();
                            }

                            input = new object[] { cmd_input_instance };
                        }

                        if (isFunctionCmd)
                        {
                            var resultObj = method.Invoke(command, input);
                            result = new ReturnMessage(true, resultObj, LocalizedString.OK);
                        }
                        else
                        {
                            method.Invoke(command, input);
                            result = new ReturnMessage(true, null, LocalizedString.OK);
                        }
                    });
                    handlemessageTask.Start();
                    await handlemessageTask.ConfigureAwait(false);

                    CleanupOldEventFiles(baseDir, TTL);
                    var eventwrapper = new ReturnMessage(new EventCallback() { id = EventId, route = callbackRoute });
                    System.IO.File.WriteAllText(baseDir + EventId.ToString() + ".event", Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    eventwrapper.Success = true;
                    RaiseEvent(eventRoute + ".succeeded", eventwrapper);

                    return result;
                }
                else
                {
                    return new ReturnMessage(false, null, LocalizedString.UnknownOperation);
                }
            }
        }

        private static void CleanupOldEventFiles(string baseDir, DateTime TTL)
        {
            if (!System.IO.Directory.Exists(baseDir))
            {
                System.IO.Directory.CreateDirectory(baseDir);
            }

            var di = new System.IO.DirectoryInfo(baseDir);
            foreach (var file in di.GetFileSystemInfos().ToList())
            {
                if (file.CreationTime < TTL)
                {
                    file.Delete();
                }
            }
        }

        private bool CheckAccess(TokenInfo tokenInfo, DateTime messageTimestamp, List<Command.Claim> requiredClaims, IResource instance)
        {
            bool hasAccess = true;
            if (requiredClaims?.Count() > 0)
            {
                hasAccess = false;
                foreach (var requiredClaim in requiredClaims)
                {
                    if (HasClaim(tokenInfo, messageTimestamp, requiredClaim, instance))
                    {
                        hasAccess = true;
                        break;
                    }
                }
            }
            return hasAccess;
        }

        /// <summary>
        /// Check if a given token has a given claim in a specific organisation
        /// </summary>
        /// <param name="token">The token to check</param>
        /// <param name="organisationContext">The organisation context to check</param>
        /// <param name="claim">The claim to check</param>
        /// <returns>true if the token has the claim</returns>
        public bool HasClaim(TokenInfo tokenInfo, DateTime messageTimestamp, Command.Claim claim, IResource instance)
        {
            if (tokenInfo == null)
            {
                return false;
            }
            if (tokenInfo.validUntil >= messageTimestamp)
            {
                if (tokenInfo.claims == null)
                {
                    return false;
                }
                else
                {
                    return tokenInfo.claims.Any(x =>
                       x.key.ToLower() == instance.Name.ToLower() + "." + claim.key.ToLower() &&
                       x.value == "true" &&
                       x.scope == claim.dataContext);
                }
            }
            else
            {
                return false;
            }
        }
    }
    public class ClaimResult
    {
        public ProgramVersion Version { get; set; }
        public ClaimDefinitions Claims { get; set; }
    }
}
