﻿using ITUtil.Common;
using ITUtil.Infrastructure.Http.Enums;
using ITUtil.Infrastructure.RabbitMQ.Message;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace ITUtil.Infrastructure.RabbitMQ.Request
{
    public class StatefullRequestClient : RequestClient
    {
        private RabbitMqHeader header = null;
        public void SetHeader(RabbitMqHeader header)
        {
            this.header = header;
        }

        public StatefullRequestClient(RabbitMqHeader header)
        {
            this.header = header;
        }

        /// <summary>
        /// Make a RPC call on a specific route, using a specific DTO input and expecting a specific DTO output
        /// </summary>
        /// <typeparam name="InputType">DTO used as input</typeparam>
        /// <typeparam name="OutputType">DTO used as output</typeparam>
        /// <param name="routingkey">The RabbitMq route for the ressource</param>
        /// <param name="data">Concrete instance of the input DTO</param>
        /// <returns>Concrete instance of the output DTO</returns>
        /// <exception cref="RestAwareException">RestAwareException</exception>
        public async Task<OutputType> RpcTask<InputType, OutputType>(string routingkey, InputType data) where InputType : class where OutputType : class
        {
            var input = JsonConvert.SerializeObject(data);
            var baseRPC = await this.Rpc(routingkey, this.header, input, string.Empty).ConfigureAwait(false);
            ThrowRestAwareErrorMessage(baseRPC);
            string jsonData = Encoding.UTF8.GetString(baseRPC.Data);
            OutputType output = JsonConvert.DeserializeObject<OutputType>(jsonData);

            return output;
        }
        /// <summary>
        /// Make a RPC call on a specific route, using a specific DTO input and expecting a specific DTO output
        /// </summary>
        /// <typeparam name="InputType">DTO used as input</typeparam>
        /// <typeparam name="OutputType">DTO used as output</typeparam>
        /// <param name="routingkey">The RabbitMq route for the ressource</param>
        /// <param name="data">Concrete instance of the input DTO</param>
        /// <returns>Concrete instance of the output DTO</returns>
        /// <exception cref="RestAwareException">RestAwareException</exception>
        public OutputType Rpc<InputType, OutputType>(string routingkey, InputType data) where InputType : class where OutputType : class
        {
            return this.RpcTask<InputType, OutputType>(routingkey, data).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Make a RPC call on a specific route, using a specific DTO input
        /// </summary>
        /// <typeparam name="InputType">DTO used as input</typeparam>
        /// <param name="routingkey">The RabbitMq route for the ressource</param>
        /// <param name="data">Concrete instance of the input DTO</param>
        /// <exception cref="RestAwareException">RestAwareException</exception>
        public async Task RpcTask<InputType>(string routingkey, InputType data) where InputType : class
        {
            var input = JsonConvert.SerializeObject(data);
            var baseRPC = await this.Rpc(routingkey, this.header, input, string.Empty).ConfigureAwait(false);
            ThrowRestAwareErrorMessage(baseRPC);
        }
        /// <summary>
        /// Make a RPC call on a specific route, using a specific DTO input
        /// </summary>
        /// <typeparam name="InputType">DTO used as input</typeparam>
        /// <param name="routingkey">The RabbitMq route for the ressource</param>
        /// <param name="data">Concrete instance of the input DTO</param>
        /// <exception cref="RestAwareException">RestAwareException</exception>
        public void Rpc<InputType>(string routingkey, InputType data) where InputType : class
        {
            this.RpcTask<InputType>(routingkey, data).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Make a RPC call on a specific route, using a specific DTO output
        /// </summary>
        /// <typeparam name="OutputType">DTO used as output</typeparam>
        /// <param name="routingkey">The RabbitMq route for the ressource</param>
        /// <returns>Concrete instance of the output DTO</returns>
        /// <exception cref="RestAwareException">RestAwareException</exception>
        public async Task<OutputType> RpcTask<OutputType>(string routingkey) where OutputType : class
        {
            var baseRPC = await this.Rpc(routingkey, this.header, string.Empty, string.Empty).ConfigureAwait(false);

            ThrowRestAwareErrorMessage(baseRPC);
            string jsonData = Encoding.UTF8.GetString(baseRPC.Data);
            OutputType output = JsonConvert.DeserializeObject<OutputType>(jsonData);

            return output;
        }
        /// <summary>
        /// Make a RPC call on a specific route, using a specific DTO output
        /// </summary>
        /// <typeparam name="OutputType">DTO used as output</typeparam>
        /// <param name="routingkey">The RabbitMq route for the ressource</param>
        /// <returns>Concrete instance of the output DTO</returns>
        /// <exception cref="RestAwareException">RestAwareException</exception>
        public OutputType Rpc<OutputType>(string routingkey) where OutputType : class
        {
            return this.RpcTask<OutputType>(routingkey).GetAwaiter().GetResult();
        }

        private void ThrowRestAwareErrorMessage(ReturnMessage message)
        {
            if (message.Success)
                return;

            if (message.Message.Text == "Missing credentials")
            {
                throw new RestAwareException(message, ErrorCode.Forbidden);
            }
            else if (message.Message.Text.StartsWith("Request timed out"))
            {
                throw new RestAwareException(message, ErrorCode.Timeout);
            }
            else if (message.Message.Text != "Error")
            {
                int errorcode = -1;
                if (int.TryParse(Encoding.UTF8.GetString(message.Data), out errorcode))
                {
                    throw new RestAwareException(message, (ErrorCode)errorcode);
                }
                throw new RestAwareException(message, ErrorCode.ServerError);
            }
            else
            {
                throw new RestAwareException(message, ErrorCode.ServerError);
            }
        }
    }
}
