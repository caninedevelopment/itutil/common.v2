﻿using ITUtil.Common;
using ITUtil.Infrastructure.Http.Enums;
using ITUtil.Infrastructure.RabbitMQ.Message;
using System;

namespace ITUtil.Infrastructure.RabbitMQ.Request
{
    public class RestAwareException : LocalizedException
    {
        public RestAwareException(ReturnMessage data, ErrorCode HTTPStatusCode) : base(data.Message is null ? data.Message.Text : "unknown error", data.Message)
        {
            this.Diagnostics = data.Diagnostics;
            this.ResponseToMessageId = data.ResponseToMessageId;
            this.Message = data.Message;
            this.Success = data.Success;
            this.Data = data.Data == null ? "" : System.Text.Encoding.UTF8.GetString(data.Data);
            this.DataType = "json";
            this.ErrorCode = HTTPStatusCode;
        }

        protected RestAwareException() : base()
        {
        }

        protected RestAwareException(string message) : base(message)
        {
        }

        protected RestAwareException(string message, Exception inner) : base(message, inner)
        {
        }

        protected RestAwareException(string message, LocalizedString error) : base(message, error)
        {
        }

        public string ResponseToMessageId { get; set; }
        public new LocalizedString Message { get; set; }
        public bool Success { get; set; }
        public string Diagnostics { get; set; }
        public string Route { get; set; }
        public new string Data { get; set; }
        public string DataType { get; set; }
        public ErrorCode ErrorCode { get; set; }
    }
}
