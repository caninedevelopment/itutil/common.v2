﻿// <copyright file="RequestClient.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Infrastructure.RabbitMQ.Request
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using global::RabbitMQ.Client;
    using global::RabbitMQ.Client.Events;
    using ITUtil.Common;
    using ITUtil.Infrastructure.Config;
    using ITUtil.Infrastructure.RabbitMQ;
    using ITUtil.Infrastructure.RabbitMQ.DTO;
    using ITUtil.Infrastructure.RabbitMQ.Message;

    /// <summary>
    /// Client for publishing messages to the request exchange
    /// </summary>
    public class RequestClient : IDisposable
    {
        private static RequestClient instance = null;

        private IConnection connection;

        private IModel channel;

        private string replyQueueName;

        private EventingBasicConsumer consumer;

        private readonly BlockingCollection<byte[]> respQueue = new BlockingCollection<byte[]>();

        private IBasicProperties props;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestClient"/> class.
        /// </summary>
        public RequestClient()
        {
            InitializeConnection();
        }

        private void InitializeConnection()
        {
            var config = GlobalRessources.GetConfig().GetSetting<RabbitMQSettings>();
            var serverparts = config.Server.Split(':');
            var factory = new ConnectionFactory()
            {
                Port = serverparts.Length > 1 ? int.Parse(serverparts[1]) : 5672,
                HostName = serverparts[0],
                UserName = config.Username,
                Password = config.Password
            };

            factory.AutomaticRecoveryEnabled = true;
            factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);
            this.connection = factory.CreateConnection();
            this.channel = this.connection.CreateModel();
            this.channel.ExchangeDeclare(exchange: ITUtil.Infrastructure.RabbitMQ.Constants.RequestExchangeName, type: "topic");
            this.replyQueueName = this.channel.QueueDeclare().QueueName;
            this.consumer = new EventingBasicConsumer(this.channel);
            this.props = this.channel.CreateBasicProperties();
            var correlationId = Guid.NewGuid().ToString();
            this.props.CorrelationId = correlationId;
            this.props.ReplyTo = this.replyQueueName;
            this.consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                if (ea.BasicProperties.CorrelationId == correlationId)
                {
                    this.respQueue.Add(body.ToArray());
                }
            };
        }

        /// <summary>
        /// Gets the singleton instance of the request client for RPC or FAF calls (for single thread applications, where we can reuse the instance)
        /// </summary>
        public static RequestClient Instance
        {
            get
            {
                return instance ?? (instance = new RequestClient());
            }
        }

        private static readonly object channelPadlock = new object();

        /// <summary>
        /// Gets the singleton instance of the request client for FAF calls (for multithreaded applications such as a web gateway, where the channel can be reused, but the instance check must be thread-safe)
        /// </summary>
        public static RequestClient FAFInstance
        {
            get
            {
                lock (channelPadlock)
                {
                    if (instance == null)
                    {
                        instance = new RequestClient();
                    }

                    return instance;
                }
            }
        }

        /// <summary>
        /// Gets a new instance of the request client for RPC calls (for multithreaded applications such as a web gateway, where each request needs its own instance)
        /// </summary>
        public static RequestClient RPCInstance
        {
            get
            {
                return new RequestClient();
            }
        }

        private const int timeoutInSeconds = 30;

        /// <summary>
        /// Make a RemoteProcedureCall, which willl only return once a result is returned from the server
        /// This overload will create a directlink message, if the messagesize is "too" big - thus, can only be used by webservers (http endpoints)
        /// </summary>
        /// <param name="routingkey">routing key</param>
        /// <param name="mw">MessageWrapper.</param>
        /// <param name="InternalServername">The url of the server with its internal-network ip, ex. https://192.68.1.1 </param>
        /// <returns>resulting object as utf8 bytearray</returns>
        public async Task<ReturnMessage> Rpc(string routingkey, RabbitMqHeader header, string messageAsJson, string InternalServername)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(messageAsJson);
            HandleLargeMessages(ref header, InternalServername, ref bytes);
            var callProps = this.channel.CreateBasicProperties();
            callProps.CorrelationId = this.props.CorrelationId;
            callProps.ReplyTo = this.props.ReplyTo;
            callProps.Headers = new Dictionary<string, object>
            {
                { "ITUtil", Newtonsoft.Json.JsonConvert.SerializeObject(header) }
            };

            this.channel.BasicPublish(
                exchange: "ms.request",
                routingKey: routingkey,
                true,//mandatory
                basicProperties: callProps,
                body: bytes);
            this.channel.BasicConsume(
                queue: this.replyQueueName,
                autoAck: true,
                consumer: this.consumer);

            for (int waitcnt = 0; waitcnt < timeoutInSeconds * 100; waitcnt++)
            {
                System.Threading.Thread.Sleep(10);

                if (this.respQueue.Count > 0)
                {
                    return await Task.Run(() =>
                    {
                        var json = System.Text.Encoding.UTF8.GetString(this.respQueue.Take());
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<ReturnMessage>(json);
                    }).ConfigureAwait(false);
                }
            }
            var message = new ReturnMessage(false, null, LocalizedString.Create(new Guid("4c3ff0e6-1aec-4918-8458-387ba4159e98"), $"Request timed out. Request was placed on queue, but no anwser was returned within the specified timeout periode: {timeoutInSeconds} seconds"));
            return await Task.Run(() => message).ConfigureAwait(false);
        }

        private static void HandleLargeMessages(ref RabbitMqHeader header, string InternalServername, ref byte[] bytes)
        {
            if (bytes.Length > 524288) //half a megabyte....
            {
                var fileName = Guid.NewGuid() + ".bin";
                var baseDir = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory() + "/directlink/");
                if (!baseDir.Exists)
                {
                    baseDir.Create();
                }
                var todaysDir = DateTime.Today.ToString("yyyy_MM_dd");

                foreach (var existingDir in baseDir.EnumerateDirectories())
                {
                    if (existingDir.Name != todaysDir)
                    {
                        existingDir.Delete(true);
                    }
                }

                var directoryName = new System.IO.DirectoryInfo(baseDir.FullName + todaysDir);
                if (!directoryName.Exists)
                {
                    directoryName.Create();
                }

                System.IO.File.WriteAllBytes(System.IO.Path.Combine(directoryName.FullName, fileName), bytes);

                header.IsDirectLink = true;

                //header = new RabbitMqHeader(
                //            header.tokenInfo,
                //            header.messageIssueDate,
                //            true,
                //            header.includeDiagnostics,
                //            header.clientId,
                //            header.messageId);

                var directLinkdata = Newtonsoft.Json.JsonConvert.SerializeObject(new DirectLink() { url = $"{InternalServername}/internal/directlink/{directoryName.Name}/{fileName}" });
                bytes = System.Text.Encoding.UTF8.GetBytes(directLinkdata);
            }
        }

        /// <summary>
        /// Fire And Forget, which just places a message on the queue (very fast!)
        /// </summary>
        /// <param name="routingkey">routingkey</param>
        /// <param name="data">data</param>
        public void FAF(string routingkey, byte[] data)
        {
            this.channel.BasicPublish(
                exchange: "ms.request",
                routingKey: routingkey,
                basicProperties: null,
                body: data);
        }

        /// <summary>
        /// Fire And Forget, which just places a message on the queue (very fast!)
        /// This overload will create a directlink message, if the messagesize is "too" big - thus, can only be used by webservers (http endpoints)
        /// </summary>
        /// <param name="routingkey">routing key</param>
        /// <param name="mw">MessageWrapper.</param>
        /// <param name="InternalServername">The url of the server with its internal-network ip, ex. https://192.68.1.1 </param>
        /// <returns>resulting object as utf8 bytearray</returns>
        public void FAF(string routingkey, RabbitMqHeader header, string messageAsJson, string InternalServername)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(messageAsJson);
            HandleLargeMessages(ref header, InternalServername, ref bytes);
            var callProps = this.channel.CreateBasicProperties();
            callProps.CorrelationId = this.props.CorrelationId;
            callProps.ReplyTo = this.props.ReplyTo;
            callProps.Headers = new Dictionary<string, object>
            {
                { "ITUtil", Newtonsoft.Json.JsonConvert.SerializeObject(header) }
            };

            this.channel.BasicPublish(
                exchange: "ms.request",
                routingKey: routingkey,
                basicProperties: callProps,
                body: bytes);
        }

        /// <summary>
        /// Close the connection to the messagequeue
        /// </summary>
        public void Close()
        {
            this.connection.Close();
        }

        public void Dispose()
        {
            channel.Close();
            channel.Dispose();
            connection.Close();
            connection.Dispose();
        }
    }
}