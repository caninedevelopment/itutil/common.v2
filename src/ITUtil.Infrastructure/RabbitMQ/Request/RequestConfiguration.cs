﻿// <copyright file="RequestConfiguration.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using ITUtil.Infrastructure.RabbitMQ.Message;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITUtil.Infrastructure.RabbitMQ.Request
{
    /// <summary>
    /// Settings for handling a specific route on the messagequeue comming in via the ms.request exchange
    /// </summary>
    public class RequestConfiguration : MessageQueueConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestConfiguration"/> class.
        /// </summary>
        /// <param name="queueName">Name of the queue onto where messages are to be placed</param>
        /// <param name="routingKey">Defines the routing of the messages that are to be placed in the queue (ex. user.#)</param>
        /// <param name="handler">A method for handling the messages / the business logic</param>
        public RequestConfiguration(string queueName, List<string> routingKeys, MessageFlow.MessageHandler handler, bool isServiceHandler = false)
            : base(Constants.RequestExchangeName, queueName, routingKeys)
        {
            this.Handler = handler;
            this.IsServiceHandler = isServiceHandler;
        }

        public bool IsServiceHandler { get; set; }

        /// <summary>
        /// Gets or sets the method for handling the messages
        /// </summary>
        public MessageFlow.MessageHandler Handler { get; set; }
    }
}
