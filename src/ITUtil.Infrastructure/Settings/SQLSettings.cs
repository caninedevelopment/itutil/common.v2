﻿// <copyright file="SQLSettings.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Infrastructure.Config
{
    using System;

    public class SQLSettings// : Config
    {
        public string Server { get; set; }
        public string DatabasePrefix { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public SQLType ServerType { get; set; }

        public string ConnectionString()
        {
            switch (this.ServerType)
            {
                case SQLType.MSSQL:
                    return this.ConnectionString("master");
                case SQLType.POSTGRESQL:
                    return this.ConnectionString("postgres");
                case SQLType.MYSQL:
                    return this.ConnectionString("mysql");
                default:
                    return this.ConnectionString("");
            }
        }

        private string GetFullDBName(string databasename)
        {
            if (databasename != "master" && databasename != "postgres" && databasename != "mysql" && databasename != "")
            {
                var prefix = DatabasePrefix == null ? "itutil" : DatabasePrefix;
                if (prefix.EndsWith("_") == false)
                {
                    prefix = prefix + "_";
                }

                return (prefix + databasename).ToLower();
            }
            else
            {
                return databasename.ToLower();
            }
        }

        public string ConnectionString(string databasename)
        {
            var serverparts = this.Server.Split(':');
            switch (this.ServerType)
            {
                case SQLType.MSSQL:
                    return $"Data Source={(serverparts.Length > 1 ? serverparts[0] + "," + serverparts[1] : serverparts[0])};Initial Catalog={GetFullDBName(databasename)};User id={this.Username};Password={this.Password}";
                case SQLType.POSTGRESQL:
                    return $"User ID={this.Username};Password={this.Password};Host={serverparts[0]};Port={(serverparts.Length > 1 ? serverparts[1] : "5432")};Database={GetFullDBName(databasename)};";
                case SQLType.MYSQL:
                    return "";
                default:
                    return "";
            }
        }

        public void CreateDatabaseIfNoExists(System.Data.IDbConnection db, string databasename)
        {
            var cmd = db.CreateCommand();
            cmd.CommandText = DatabaseExistsSQL(GetFullDBName(databasename));
            var res = cmd.ExecuteScalar();
            long cnt = Convert.ToInt64(res);

            if (cnt <= 0)
            {
                var create_cmd = db.CreateCommand();
                create_cmd.CommandText = $"CREATE DATABASE {GetFullDBName(databasename)};";
                create_cmd.ExecuteNonQuery();
            }
        }

        private string DatabaseExistsSQL(string databasename)
        {
            switch (this.ServerType)
            {
                case SQLType.MSSQL:
                    return $"select count(*) from master.dbo.sysdatabases where name = '{databasename}'";
                case SQLType.POSTGRESQL:
                    return $"SELECT count(*) FROM pg_database WHERE datname = '{databasename}'";
                case SQLType.MYSQL:
                    return $"SELECT count(*) FROM information_schema.schemata WHERE schema_name = '{databasename}'";
                default:
                    return "";
            }
        }

        public enum SQLType
        {
            MSSQL, POSTGRESQL, MYSQL
        }
    }
}