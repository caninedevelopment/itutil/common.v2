﻿// <copyright file="GlobalRessources.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Infrastructure.Config
{
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;
    public class GlobalRessources
    {
        public Dictionary<string, JObject> Configs { get; set; }

        public static GlobalRessources GetConfig(string settingsFilename = null)
        {
            string filename = Directory.GetCurrentDirectory() + "/" + (string.IsNullOrEmpty(settingsFilename) ? "appsettings.json" : settingsFilename);

            if (File.Exists(filename))
            {
                string json = File.ReadAllText(filename);
                var globalResources = Newtonsoft.Json.JsonConvert.DeserializeObject<GlobalRessources>(json);

                return globalResources;
            }

            return null;
        }

        /// <summary>
        /// please use ITUtil.Config.Implementation
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetSetting<T>()
        {
            return this.Configs[typeof(T).Name].ToObject<T>();
        }
    }
}
