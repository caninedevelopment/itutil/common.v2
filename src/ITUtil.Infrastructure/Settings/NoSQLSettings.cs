﻿// <copyright file="SQLSettings.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Infrastructure.Config
{
    using System.Collections.Generic;
    public class NoSQLSettings
    {
        public List<string> Servers { get; set; }
        public string DatabasePrefix { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
        public NoSQLType ServerType { get; set; }

        public string GetFullDBName(string databasename)
        {
            var prefix = DatabasePrefix ?? "itutil";
            if (prefix.EndsWith("_") == false)
            {
                prefix += "_";
            }

            return (prefix + databasename).ToLower();
        }

        public string ConnectionString()
        {
            switch (this.ServerType)
            {
                case NoSQLType.MONGODB:
                    string encodedCredentials = "";
                    if (!string.IsNullOrEmpty(this.Username) && !string.IsNullOrEmpty(this.Password))
                    {
                        encodedCredentials = System.Web.HttpUtility.UrlEncode(this.Username) + ":" + System.Web.HttpUtility.UrlEncode(this.Password) + "@";
                    }
                    return $"mongodb://{encodedCredentials}{string.Join(",", this.Servers)}";
                default:
                    return "";
            }
        }

        public enum NoSQLType
        {
            MONGODB
        }
    }
}