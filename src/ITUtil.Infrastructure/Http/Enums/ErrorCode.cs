﻿namespace ITUtil.Infrastructure.Http.Enums
{
    public enum ErrorCode
    {
        NoContent = 204,
        BadRequest = 400,
        Forbidden = 403,
        RessourceNotFound = 404,
        Timeout = 408,
        Conflict = 409,
        ServerError = 500
    }
}
