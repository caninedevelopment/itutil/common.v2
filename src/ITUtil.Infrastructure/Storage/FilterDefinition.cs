﻿namespace ITUtil.Infrastructure.Storage
{
    using System.Collections.Generic;
    using ITUtil.Common.Command.Interfaces;

    public interface IFilterDefinition : IDtoRequest
    {
        StatementOperator Operators { get; set; }//TODO: tidligere blev serialisering/deserialisering issue løst med nedenstående, hvordan sikre vi os at det fortsat fungere?

        //public FilterDefinition()
        //{
        //Operators = new StatementOperator();
        //}

        ///// <summary>
        ///// return only rows/documents that comply with all filters in this list (i.e. AND operation)
        ///// </summary>
        //public List<Operator> EntityFilters 
        //{
        //    get { return Operators.Statements; } 
        //    set { Operators.Statements = value; } 
        //}

        //public string ToString()
        //{
        //    return Operators.ToString();
        //}
        //public void Validate()
        //{
        //    if (EntityFilters == null)
        //    {
        //        throw new Base.NullOrDefaultException(nameof(this.EntityFilters));
        //    }

        //    Operators.Validate();
        //}
    }
}
