﻿//using ITUtil.Common;
//using System;
//using System.Collections.Generic;
//using System.Linq.Expressions;

//namespace ITUtil.Infrastructure.Storage
//{
//    public class RepositoryValidationExtention<TEntity> : IRepository<TEntity> where TEntity : IBaseEntity
//    {
//        private IRepository<TEntity> repository = null;

//        private string ressourceName;

//        //public List<FieldFilter> PrimaryKey { get { return this.repository.PrimaryKey; } }

//        //public FilterDefinition GetById(TEntity entity) 
//        //{ 
//        //    return this.repository.GetById(entity); 
//        //}
//        //public byte[] GetHash(TEntity entity)
//        //{
//        //    return this.repository.GetHash(entity);
//        //}
//        //public byte[] GetHash(FilterDefinition filter)
//        //{
//        //    return this.repository.GetHash(filter);
//        //}

//        public RepositoryValidationExtention(IRepository<TEntity> repository, string ressourceName)
//        {
//            this.repository = repository;
//            this.ressourceName = ressourceName;
//        }

//        //private void ValidateFieldFilter(FieldDefinition fieldFilter)
//        //{
//        //    if (fieldFilter != null && fieldFilter.ExcludeFields.Count > 0 && fieldFilter.IncludeFields.Count > 0)
//        //    {
//        //        throw new ValidationException("IncludeFields and ExcludeFields are mutual exclusive", nameof(fieldFilter));
//        //    }
//        //}

//        //public void RecreateRepository()
//        //{
//        //    repository.RecreateRepository();
//        //}

//        //public TEntity Get(FilterDefinition filter, FieldDefinition fieldFilter = null)
//        //{
//        //    this.ValidateFieldFilter(fieldFilter);

//        //    if (filter == null || filter.EntityFilters.Count == 0)
//        //    {
//        //        throw new ValidationException("No filter was provided - please provide filter that returns exactly one element");
//        //    }

//        //    var entity = repository.Get(filter, fieldFilter);
//        //    if (entity == null)
//        //    {
//        //        throw new ElementDoesNotExistException($"{this.ressourceName} does not exist", filter.ToString());
//        //    }

//        //    return entity;
//        //}

//        //public PageResult<TEntity> GetMany(FilterDefinition filter, PagingDefinition paging, FieldDefinition fieldFilter = null)
//        //{
//        //    this.ValidateFieldFilter(fieldFilter);
//        //    return repository.GetMany(filter, paging, fieldFilter);
//        //}

//        //public void Insert(TEntity instance)
//        //{
//        //    var filter = repository.GetById(instance);

//        //    var entity = repository.Get(filter);
//        //    if (entity != null)
//        //    {
//        //        throw new ElementAlreadyExistsException($"{this.ressourceName} already exists", filter.ToString());
//        //    }

//        //    repository.Insert(instance);
//        //}

//        //public void Update(TEntity instance)
//        //{
//        //    var filter = repository.GetById(instance);

//        //    var entity = repository.Get(filter);
//        //    if (entity == null)
//        //    {
//        //        throw new ElementDoesNotExistException($"{this.ressourceName} does not exist", filter.ToString());
//        //    }

//        //    repository.Update(instance);
//        //}

//        //public void Delete(TEntity instance)
//        //{
//        //    var filter = repository.GetById(instance);

//        //    var entity = repository.Get(filter);
//        //    if (entity == null)
//        //    {
//        //        throw new ElementDoesNotExistException($"{this.ressourceName} does not exist", filter.ToString());
//        //    }

//        //    repository.Delete(entity);
//        //}

//        //public void DeleteMany(FilterDefinition filter)
//        //{
//        //    foreach (var entity  in repository.GetMany(filter, new PagingDefinition() { PageSize = int.MaxValue }).Results)
//        //    {
//        //        this.Delete(entity);
//        //    }
//        //}
//        public void Delete()
//        {
//            repository.Delete();
//        }

//        public TEntity Get()
//        {
//            return repository.Get();
//        }
//        public TResult Get<TResult>(Expression<Func<TEntity, TResult>> selector)
//        {
//            return repository.Get(selector);
//        }
//        public List<TEntity> GetMany()
//        {
//            return repository.GetMany();
//        }
//        public List<TResult> GetMany<TResult>(Expression<Func<TEntity, TResult>> selector)
//        {
//            return repository.GetMany(selector);
//        }

//        public void Insert(TEntity instance)
//        {
//            repository.Insert(instance);
//        }

//        public void Update(object input)
//        {
//            repository.Update(input);
//        }

//        public IFiltered<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
//        {
//            return repository.Where(predicate);
//        }
//    }
//}

