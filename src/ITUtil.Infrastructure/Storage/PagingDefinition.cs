﻿namespace ITUtil.Infrastructure.Storage
{
    using ITUtil.Common.Command.Interfaces;

    public enum OrderDirection
    {
        Ascending,
        Descending
    }

    public interface IPagingDefinition : IDtoRequest
    {
        //public static PagingDefinition Default
        //{
        //    get
        //    {
        //        return new PagingDefinition()
        //        {
        //            OrderByDirection = PagingDefinition.OrderDirection.Ascending,
        //            OrderByField = string.Empty,
        //            PageNumber = 1,
        //            PageSize = 100
        //        };
        //    }
        //}

        uint PageSize { get; set; }
        uint PageNumber { get; set; }

        string OrderByField { get; set; }
        OrderDirection OrderByDirection { get; set; }

        //public void Validate()
        //{
        //    if (PageSize <= 0)
        //    {
        //        throw new Base.NullOrDefaultException("Pagesize must be higher than 0", nameof(PageSize));
        //    }
        //}
    }
}
