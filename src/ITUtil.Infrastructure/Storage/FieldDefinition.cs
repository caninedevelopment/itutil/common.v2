﻿using ITUtil.Common.Command.Interfaces;
using System.Collections.Generic;

namespace ITUtil.Infrastructure.Storage
{
    public interface IFieldDefinition : IDtoRequest
    {
        //public FieldDefinition()
        //{
        //    IncludeFields = new List<FieldFilter>();
        //    ExcludeFields = new List<FieldFilter>();
        //}
        /// <summary>
        /// return only data from these fields, empty list equals ignore (take all)
        /// </summary>
        List<string> FieldNames { get; set; }

        ///// <summary>
        ///// do not return data from these fields, empty list equals ignore (take all)
        ///// </summary>
        //public List<FieldFilter> ExcludeFields { get; set; }

        //public void Validate()
        //{
        //}
    }
}
