﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ITUtil.Infrastructure.Storage
{
    public class CompareOperator : Operator
    {
        public enum OperatorEnum
        {
            Equals = 10,
            GreaterThan = 20,
            LessThan = 30,
            Contains = 40,
            StartsWith = 50,
            EndsWith = 60
        }
        public string FieldName { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public OperatorEnum Operator { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return $"{this.FieldName} {this.Operator} {this.Value}";
        }
        public override void Validate()
        {
        }
    }
}
