﻿using Newtonsoft.Json;

namespace ITUtil.Infrastructure.Storage
{
    [JsonConverter(typeof(OperatorConverter))]
    public abstract class Operator
    {
        public virtual void Validate() {; }
    }
}
