﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ITUtil.Infrastructure.Storage
{
    public class LogicOperator : Operator
    {
        public enum AndOrEnum
        {
            And = 20,
            Or = 30
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public AndOrEnum AndOr { get; set; }
        public override string ToString()
        {
            return $"{this.AndOr}";
        }
        public override void Validate()
        {
        }
    }
}
