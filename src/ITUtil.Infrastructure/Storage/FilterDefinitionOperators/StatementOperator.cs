﻿using ITUtil.Common;
using System.Collections.Generic;
using System.Text;

namespace ITUtil.Infrastructure.Storage
{
    public class StatementOperator : Operator
    {
        public StatementOperator()
        {
            Statements = new List<Operator>();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var op in Statements)
            {
                sb.Append(op.ToString());
            }
            return sb.ToString();
        }

        public List<Operator> Statements { get; set; }
        public override void Validate()
        {
            if (Statements.Count % 2 == 0)
            {
                throw new ValidationException("Every second operation must be a logic operator, the first and the last operation can never be a logic operator");
            }

            for (int i = 0; i < Statements.Count; i++)
            {
                if (Statements[i] is LogicOperator && i % 2 == 0)
                {
                    throw new ValidationException("Every second operation must be a logic operator, the first and the last operation can never be a logic operator");
                }
            }

            foreach (var s in this.Statements)
            {
                s.Validate();
            }
        }
    }
}
