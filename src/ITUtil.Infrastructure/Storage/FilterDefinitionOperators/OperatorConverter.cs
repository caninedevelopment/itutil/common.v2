﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace ITUtil.Infrastructure.Storage
{
    public class OperatorConverter : JsonCreationConverter<Operator>
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        protected override Operator Create(Type objectType, JObject jObject)
        {
            if (FieldExists("$type", jObject))
            {
                switch (jObject["$type"].Value<string>())
                {
                    case "LogicOperator":
                    case "ITUtil.Common.Storage.LogicOperator, ITUtil.Common":
                        return new LogicOperator();
                    case "InOperator":
                    case "ITUtil.Common.Storage.InOperator, ITUtil.Common":
                        return new InOperator();
                    case "CompareOperator":
                    case "ITUtil.Common.Storage.CompareOperator, ITUtil.Common":
                        return new CompareOperator();
                    case "StatementOperator":
                    case "ITUtil.Common.Storage.StatementOperator, ITUtil.Common":
                        return new StatementOperator();
                    case "FieldCompareOperator":
                    case "ITUtil.Common.Storage.FieldCompareOperator, ITUtil.Common":
                        return new FieldCompareOperator();
                    default:
                        throw new Exception("Unknown");
                }
            }
            throw new Exception("Unknown");
        }

        private bool FieldExists(string fieldName, JObject jObject)
        {
            return jObject[fieldName] != null;
        }
    }
}
