﻿using ITUtil.Common;
using System.Collections.Generic;

namespace ITUtil.Infrastructure.Storage
{
    public class InOperator : Operator
    {
        public InOperator()
        {
            Values = new List<string>();
        }
        public List<string> Values { get; set; }
        public string FieldName { get; set; }
        public override string ToString()
        {
            return $"{this.FieldName} in ({string.Join(",", this.Values)})";
        }
        public override void Validate()
        {
            if (Values.Count == 0)
            {
                throw new ValidationException("InOperators must have at least 1 value");
            }
        }
    }
}
