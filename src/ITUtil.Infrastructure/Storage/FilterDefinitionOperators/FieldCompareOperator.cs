﻿using ITUtil.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ITUtil.Infrastructure.Storage
{
    public class FieldCompareOperator : Operator
    {
        public enum OperatorEnum
        {
            Equals = 10,
            GreaterThan = 20,
            LessThan = 30,
            Contains = 40,
            StartsWith = 50,
            EndsWith = 60
        }
        public string LeftFieldName { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public OperatorEnum Operator { get; set; }
        public string RightFieldName { get; set; }

        public override string ToString()
        {
            return $"{this.LeftFieldName} {this.Operator} {this.RightFieldName}";
        }

        public override void Validate()
        {
            if (LeftFieldName == RightFieldName)
            {
                throw new ValidationException("Left and Right fieldname must differ");
            }
            if (string.IsNullOrEmpty(LeftFieldName))
            {
                throw new ValidationException("LeftFieldName is mandatory");
            }
            if (string.IsNullOrEmpty(RightFieldName))
            {
                throw new ValidationException("RightFieldName is mandatory");
            }
        }
    }
}
