﻿//using System;
//using System.Collections.Generic;

//namespace ITUtil.Infrastructure.Storage
//{
//    public class RepositoryLogDateExtention<TEntity> : IRepository<TEntity> where TEntity : IBaseEntity
//    {
//        protected IRepository<TEntity> repository = null;

//        public List<FieldFilter> PrimaryKey { get { return this.repository.PrimaryKey; } }
//        public FilterDefinition GetById(TEntity entity)
//        { 
//            return this.repository.GetById(entity); 
//        }
//        public byte[] GetHash(TEntity entity)
//        {
//            return this.repository.GetHash(entity);
//        }
//        public byte[] GetHash(FilterDefinition filter)
//        {
//            return this.repository.GetHash(filter);
//        }
//        public RepositoryLogDateExtention(IRepository<TEntity> repository)
//        {
//            this.repository = repository;
//        }
//        public virtual void Delete(TEntity instance)
//        {
//            instance.DeletedDate = DateTime.Now;
//            instance.ModifiedDate = instance.DeletedDate.Value;
//            repository.Update(instance);
//        }
//        public virtual void DeleteMany(FilterDefinition filter)
//        {
//            foreach (var entity in repository.GetMany(filter, new PagingDefinition() { PageSize = int.MaxValue }).Results)
//            {
//                this.Delete(entity);
//            }
//        }
//        public virtual TEntity Get(FilterDefinition filter, FieldDefinition fieldFilter = null)
//        {
//            return repository.Get(filter, fieldFilter);
//        }

//        public virtual PageResult<TEntity> GetMany(FilterDefinition filter, PagingDefinition paging, FieldDefinition fieldFilter = null)
//        {
//            return repository.GetMany(filter, paging, fieldFilter);
//        }
//        public virtual void Insert(TEntity instance)
//        {
//            instance.CreatedDate = DateTime.Now;
//            instance.ModifiedDate = instance.CreatedDate;
//            repository.Insert(instance);
//        }
//        public void RecreateRepository()
//        {
//            repository.RecreateRepository();
//        }
//        public virtual void Update(TEntity instance)
//        {
//            instance.ModifiedDate = DateTime.Now;
//            repository.Update(instance);
//        }
//    }
//}