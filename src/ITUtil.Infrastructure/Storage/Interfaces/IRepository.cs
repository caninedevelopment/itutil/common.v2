﻿//using System;
//using System.Collections.Generic;
//using System.Linq.Expressions;

//namespace ITUtil.Infrastructure.Storage
//{
//    public interface IRetrievable<TSource>
//    {
//        TSource Get();
//        TResult Get<TResult>(Expression<Func<TSource, TResult>> selector);
//        List<TSource> GetMany();
//        List<TResult> GetMany<TResult>(Expression<Func<TSource, TResult>> selector);
//    }

//    public interface IModifyable<TSource>
//    {
//        void Update(object input);

//        void Delete();
//    }

//    public interface IFiltered<TSource> : IModifyable<TSource>, IRetrievable<TSource>
//    {
//    }

//    public interface IFilterAble<TSource>
//    {
//        IFiltered<TSource> Where(Expression<Func<TSource, bool>> predicate);
//    }

//    public interface IRepository<TSource> : IRetrievable<TSource>, IFilterAble<TSource>, IModifyable<TSource>, IFiltered<TSource> where TSource : IBaseEntity
//    {
//        void Insert(TSource instance);
//        //void RecreateRepository();
//    }
//}
