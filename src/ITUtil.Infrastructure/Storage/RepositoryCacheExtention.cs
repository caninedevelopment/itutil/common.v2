﻿//using ITUtil.Common.Base;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace ITUtil.Infrastructure.Storage
//{

//    public class PagingSort<TEntity> : IComparer<TEntity>
//    {
//        public PagingSort(PagingDefinition pageingDefinition)
//        {
//            this.pageingDefinition = pageingDefinition;
//        }
//        public PagingDefinition pageingDefinition { get; }

//        public int Compare(TEntity x, TEntity y)
//        {
//            var xval = x.GetType().GetProperty(pageingDefinition.OrderByField).GetValue(x, null);
//            var yval = y.GetType().GetProperty(pageingDefinition.OrderByField).GetValue(y, null);

//            if (xval == null || yval == null)
//                return 0;

//            return xval.ToString().CompareTo(yval.ToString());//TODO: datatyper?!
//        }
//    }
//    public class RepositoryCacheExtention<TEntity> : RepositoryLogDateExtention<TEntity>, IRepository<TEntity> where TEntity : IBaseEntity
//    {
//        private void filter(List<TEntity> entities, List<Operator> operators)
//        {//TODO: Flyt ud i noget generisk kode?
//            foreach (var op in operators)
//            {
//                if (op is CompareOperator)
//                {
//                    CompareOperator cop = op as CompareOperator;

//                    for (int i = entities.Count-1; i >= 0 ; i--)
//                    {
//                        var val = entities[i].GetType().GetProperty(cop.FieldName).GetValue(entities[i], null).ToString();//TODO: NULL håndtering
//                        switch (cop.Operator)
//                        {
//                            case CompareOperator.OperatorEnum.Equals:
//                                if (cop.Value != val)
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            //TODO:
//                            //case CompareOperator.OperatorEnum.GreaterThan:
//                            //    if (cop.Value > val)//datatype!?
//                            //    {
//                            //        entities.RemoveAt(i);
//                            //    }
//                            //    break;
//                            //case CompareOperator.OperatorEnum.LessThan:
//                            //    if (cop.Value < val)//datatype!?
//                            //    {
//                            //        entities.RemoveAt(i);
//                            //    }
//                            //    break;
//                            case CompareOperator.OperatorEnum.Contains:
//                                if (val.Contains(cop.Value) == false)
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            case CompareOperator.OperatorEnum.StartsWith:
//                                if (val.StartsWith(cop.Value) == false)
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            case CompareOperator.OperatorEnum.EndsWith:
//                                if (val.EndsWith(cop.Value)==false)//datatype!?
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            default:
//                                break;
//                        }
//                    }
//                }
//                if (op is FieldCompareOperator)
//                {
//                    FieldCompareOperator fcop = op as FieldCompareOperator;

//                    for (int i = entities.Count - 1; i >= 0; i--)
//                    {
//                        var valLeft = entities[i].GetType().GetProperty(fcop.LeftFieldName).GetValue(entities[i], null).ToString();//TODO: NULL håndtering
//                        var valRigth = entities[i].GetType().GetProperty(fcop.RightFieldName).GetValue(entities[i], null).ToString();//TODO: NULL håndtering
//                        switch (fcop.Operator)
//                        {
//                            case FieldCompareOperator.OperatorEnum.Equals:
//                                if (valLeft != valRigth)
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            //TODO:
//                            //case FieldCompareOperator.OperatorEnum.GreaterThan:
//                            //    if (valLeft > valRigth)//datatype!?
//                            //    {
//                            //        entities.RemoveAt(i);
//                            //    }
//                            //    break;
//                            //case FieldCompareOperator.OperatorEnum.LessThan:
//                            //    if (valLeft < valRigth)//datatype!?
//                            //    {
//                            //        entities.RemoveAt(i);
//                            //    }
//                            //    break;
//                            case FieldCompareOperator.OperatorEnum.Contains:
//                                if (valLeft.Contains(valRigth) == false)
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            case FieldCompareOperator.OperatorEnum.StartsWith:
//                                if (valLeft.StartsWith(valRigth) == false)
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            case FieldCompareOperator.OperatorEnum.EndsWith:
//                                if (valLeft.EndsWith(valRigth) == false)//datatype!?
//                                {
//                                    entities.RemoveAt(i);
//                                }
//                                break;
//                            default:
//                                break;
//                        }
//                    }
//                }
//                if (op is InOperator)
//                {
//                    InOperator iop = op as InOperator;
//                    //iop.FieldName
//                    for (int i = entities.Count - 1; i >= 0; i--)
//                    {
//                        var val = entities[i].GetType().GetProperty(iop.FieldName).GetValue(entities[i], null).ToString();//TODO: NULL håndtering

//                        if (iop.Values.Contains(val) == false)
//                        {
//                            entities.RemoveAt(i);
//                        }
//                    }
//                }
//                if (op is LogicOperator)
//                {
//                    LogicOperator lop = op as LogicOperator;
//                    //how to?
//                }

//                if (op is StatementOperator)
//                {
//                    filter(entities, (op as StatementOperator).Statements);
//                }
//            }

//        }

//        private int maxsize;
//        private Dictionary<byte[], TEntity> cache = new Dictionary<byte[], TEntity>();
//        public RepositoryCacheExtention(IRepository<TEntity> repository, int maxsize) : base(repository)
//        {
//            this.maxsize = maxsize;
//        }

//        public override void Delete(TEntity instance)
//        {
//            cache.Remove(base.GetHash(instance));
//            base.Delete(instance);
//        }

//        public override TEntity Get(FilterDefinition filter, FieldDefinition fieldFilter = null)
//        {
//            var key = this.GetHash(filter);
//            TEntity entity = default(TEntity);
//            if (cache.TryGetValue(key, out entity))
//            {//check if we have newer in the database
//                filter.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.And }); //TODO: Problematisk at vi ændre på filter? (det er REF)
//                filter.EntityFilters.Add(new CompareOperator() //TODO: problematik: hvad hvis søgning ikke er på primær nøgle? 
//                { 
//                    FieldName = nameof(IBaseEntity.ModifiedDate), 
//                    Operator = CompareOperator.OperatorEnum.GreaterThan, 
//                    Value = entity.ModifiedDate.ToString() //TODO: HOW!? 
//                });

//                var fromDB = this.repository.Get(filter);//TODO: ISSUE, hvis der er validation på vil denne give exception!
//                if (fromDB != null)
//                {
//                    UpdateCache(fromDB, key);
//                    return fromDB;
//                } else
//                {
//                    return entity;
//                }
//            } else
//            {
//                var fromDB = this.repository.Get(filter);//TODO: ISSUE, hvis der er validation på vil denne give exception!
//                if (fromDB != null)
//                {
//                    UpdateCache(fromDB, key);
//                    return fromDB;
//                }
//            }
//            return default(TEntity); 
//        }

//        public override PageResult<TEntity> GetMany(FilterDefinition filter, PagingDefinition paging, FieldDefinition fieldFilter = null)
//        {
//            var allFromDB = this.repository.GetMany(filter, new PagingDefinition() { PageSize = uint.MaxValue }, new FieldDefinition() { IncludeFields = PrimaryKey }).Results;

//            var cachecopy = this.cache.Values.ToList();
//            this.filter(cachecopy, filter.EntityFilters);
//            var lastdateInCache = cachecopy.Max(p => p.ModifiedDate);

//            foreach (var fromCache in cachecopy)
//            {
//                var fromCacheKey = this.repository.GetHash(fromCache);

//                foreach (var fromDB in allFromDB)
//                {
//                    var fromDbKey = this.repository.GetHash(fromDB);
//                    if (fromDbKey == fromCacheKey)
//                    {
//                        if (fromCache.ModifiedDate < fromDB.ModifiedDate)
//                        {
//                            var reloadfromDB = this.repository.Get(this.GetById(fromCache));
//                            //TODO: cachecopy skal unloade "fromCache" og istedet have "reloadFromDB"
//                            UpdateCache(reloadfromDB, fromCacheKey);
//                        }
//                        break;
//                    }
//                }
//            }
//            //LOAD ALL NEWER FROM DB
//            filter.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.And }); //TODO: Problematisk at vi ændre på filter? (det er REF)
//            filter.EntityFilters.Add(new CompareOperator()
//            {
//                FieldName = nameof(IBaseEntity.ModifiedDate),
//                Operator = CompareOperator.OperatorEnum.GreaterThan,
//                Value = lastdateInCache.ToString() //TODO HOW?
//            });

//            foreach (var fromDB in this.repository.GetMany(filter, new PagingDefinition() { PageSize = uint.MaxValue }, new FieldDefinition() { }).Results)
//            {
//                AddToCache(repository.GetHash(fromDB), fromDB);
//                cachecopy.Add(fromDB);
//            }

//            var ps = new Storage.PagingSort<TEntity>(paging);

//            cachecopy.Sort(ps);
//            var result = cachecopy.Skip((int)paging.PageSize * ((int)paging.PageNumber - 1)).Take((int)paging.PageSize);

//            return new PageResult<TEntity>() { NumberOfResults = (ulong)cachecopy.Count(), PageNumber = paging.PageNumber, PageSize = paging.PageSize, Results = result };
//        }

//        public override void Insert(TEntity instance)
//        {
//            base.Insert(instance);
//            AddToCache(base.GetHash(instance), instance);
//        }

//        public override void Update(TEntity instance)
//        {
//            var key = base.GetHash(instance);
//            base.Update(instance);

//            UpdateCache(instance, key);
//        }

//        private void UpdateCache(TEntity instance, byte[] key)
//        {
//            if (cache.ContainsKey(key))
//            {
//                cache[key] = instance;
//            }
//            else
//            {
//                AddToCache(key, instance);
//            }
//        }

//        private void AddToCache(byte[] key, TEntity entity)
//        {
//            cache.Add(key, entity);
//            if (cache.Count > this.maxsize)
//            {
//                DateTime oldest = DateTime.Now;
//                byte[] oldestKey = null;
//                foreach (var val in cache.Values)
//                {
//                    if (val.ModifiedDate < oldest)//TODO: last retreived date from cache instead...
//                    {
//                        oldestKey = base.GetHash(entity);
//                        oldest = val.ModifiedDate;
//                    }
//                }
//                if (oldestKey != null)
//                {
//                    cache.Remove(oldestKey);
//                }
//            }
//        }
//    }
//}
