## Introduction
These common libs represents the dotnet core C# implementation of the ITutil microservice platform.
The are avaiable as nuget packages at https://gitlab.com/api/v4/projects/17035152/packages/nuget/index.json 

# Reference links

- [GitLab packages](https://gitlab.com/api/v4/projects/17035152/packages/nuget/index.json)
- [ITutil homepage](https://www.monosoft.dk/itutil/)

## Update nuget

Pleaes update the packages with the following commands:

nuget push ./ITUtil.Common.Base/bin/Debug/ITUtil.Common.Base.2.0.0.X.nupkg -Source ITUtil 

nuget push ./ITUtil.Common.Command/bin/Debug/ITUtil.Common.Command.2.0.0.X.nupkg -Source ITUtil 

nuget push ./ITUtil.Common.Console/bin/Debug/ITUtil.Common.Console.2.0.0.X.nupkg -Source ITUtil 

nuget push ./ITUtil.Common.RabbitMQ/bin/Debug/ITUtil.Common.RabbitMQ.2.0.0.X.nupkg -Source ITUtil

nuget push ./ITUtil.Common.Settings/bin/Debug/ITUtil.Common.Config.2.0.0.X.nupkg -Source ITUtil


