﻿using ITUtil.Common.Command;

namespace ITUtil.Common.Unittest
{
    using ITUtil.Common.Command.Interfaces;
    public class Execute_outputonly_Function : IFunction<SimpleOutput>
    {
        public SimpleOutput Execute()
        {
            return new SimpleOutput() { output = string.Empty };
        }
    }
}
