﻿namespace ITUtil.Common.Unittest
{
    using ITUtil.Common.Command.Interfaces;

    public class Execute_inputoutput_Function : IFunction<SimpleInput, SimpleOutput>
    {
        public SimpleOutput Execute(SimpleInput input)
        {
            return new SimpleOutput() { output = input.input };
        }
    }
}
