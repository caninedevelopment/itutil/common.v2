﻿using ITUtil.Common.Command;
using ITUtil.Infrastructure.Console;
using System;
using System.Collections.Generic;
using System.Text;

namespace ITUtil.Common.Unittest
{
    public class TestResource : BaseResource
    {
        public TestResource() : base("UnittestRessource", new ProgramVersion(1))
        {
            this.AddCommand(new Execute_inputoutput_Function(), ITUtilOperationType.Insert, "Insert", "Description");
            this.AddCommand(new Execute_outputonly_Function(), ITUtilOperationType.Get, "Get", "Description");
            this.AddCommand(new Execute_inputonly_Procedure(), ITUtilOperationType.Insert, "Insert", "Description");
            this.AddCommand(new Execute_noinputnooutput_Procedure(), ITUtilOperationType.Insert, "Insert", "Description");
        }
    }
}
