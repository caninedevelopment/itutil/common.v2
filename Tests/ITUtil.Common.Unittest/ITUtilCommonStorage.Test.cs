﻿//using ITUtil.Common.Command;
//using NUnit.Framework;
//using Newtonsoft.Json;
//using System.Collections.Generic;
//using ITUtil.Common.Storage;
//using System;

//namespace ITUtil.Common.Unittest
//{
//    [TestFixture]
//    public class ConsoleProgramTests
//    {
//        [Test]
//        public void FilterDefinition_Basic_JsonSerializationTest()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });

//            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.None, TypeNameHandling = TypeNameHandling.Objects };
//            var txt = JsonConvert.SerializeObject(fd, settings);
//            var resurected = JsonConvert.DeserializeObject<FilterDefinition>(txt);

//            Assert.AreEqual(fd.EntityFilters.Count, resurected.EntityFilters.Count);
//            Assert.AreEqual(fd.EntityFilters[0].GetType(), resurected.EntityFilters[0].GetType());
//            Assert.AreEqual(fd.EntityFilters[1].GetType(), resurected.EntityFilters[1].GetType());
//            Assert.AreEqual(fd.EntityFilters[2].GetType(), resurected.EntityFilters[2].GetType());
//        }
//        [Test]
//        public void FilterDefinition_WithSubStatement_JsonSerializationTest()
//        {
//            var json = "{\"EntityFilters\":[{\"$type\":\"CompareOperator\",\"FieldName\":\"FirstName\",\"Operator\":\"Equals\",\"Value\":\"Jimmy\"},{\"$type\":\"LogicOperator\",\"AndOr\":\"Or\"},{\"$type\":\"CompareOperator\",\"FieldName\":\"LastName\",\"Operator\":\"Equals\",\"Value\":\"Borch\"},{\"$type\":\"LogicOperator\",\"AndOr\":\"Or\"},{\"$type\":\"StatementOperator\",\"Statements\":[{\"$type\":\"CompareOperator\",\"FieldName\":\"FirstName\",\"Operator\":\"Equals\",\"Value\":\"Jimmy\"}]}]}\"";
//            var cmd_input_instance = Activator.CreateInstance(typeof(FilterDefinition));
//            Newtonsoft.Json.JsonConvert.PopulateObject(json, cmd_input_instance);

//            (cmd_input_instance as FilterDefinition).Validate();
//        }
//        [Test]
//        public void DeserializedJson_Simple_Test()
//        {
//            var json = "{\"EntityFilters\":[{\"$type\":\"CompareOperator\",\"FieldName\":\"FirstName\",\"Operator\":\"Equals\",\"Value\":\"Jimmy\"},{\"$type\":\"LogicOperator\",\"AndOr\":\"Or\"},{\"$type\":\"CompareOperator\",\"FieldName\":\"LastName\",\"Operator\":\"Equals\",\"Value\":\"Borch\"},{\"$type\":\"LogicOperator\",\"AndOr\":\"Or\"},{\"$type\":\"InOperator\",\"Values\":[\"Jimmy\",\"Borch\"],\"FieldName\":\"LastName\"}]}\"";


//            var cmd_input_instance = Activator.CreateInstance(typeof(FilterDefinition));
//            Newtonsoft.Json.JsonConvert.PopulateObject(json, cmd_input_instance);

//            (cmd_input_instance as FilterDefinition).Validate();
//        }


//        [Test]
//        public void FilterDefinition_WithInStatement_JsonSerializationTest()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new InOperator() {  FieldName = "LastName", Values = new List<string>() { "Jimmy", "Borch" } });

//            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.None, TypeNameHandling = TypeNameHandling.Objects };
//            var txt = JsonConvert.SerializeObject(fd, settings);
//            var resurected = JsonConvert.DeserializeObject<FilterDefinition>(txt);

//            Assert.AreEqual(fd.EntityFilters.Count, resurected.EntityFilters.Count);
//        }

//        [Test]
//        public void FilterDefinition_WithMultipleLevels_JsonSerializationTest()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new InOperator() { FieldName = "LastName", Values = new List<string>() { "Jimmy", "Borch" } });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            var substatement = new StatementOperator();
//            substatement.Statements.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            substatement.Statements.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            var subsubstatement = new StatementOperator();
//            subsubstatement.Statements.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            substatement.Statements.Add(subsubstatement);
//            fd.EntityFilters.Add(substatement);



//            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.None, TypeNameHandling = TypeNameHandling.Objects };
//            var txt = JsonConvert.SerializeObject(fd, settings);
//            var resurected = JsonConvert.DeserializeObject<FilterDefinition>(txt);

//            Assert.AreEqual(fd.EntityFilters.Count, resurected.EntityFilters.Count);
//        }

//        [Test]
//        public void FilterDefinition_Valid_Validation()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new InOperator() { FieldName = "LastName", Values = new List<string>() { "Jimmy", "Borch" } });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            var substatement = new StatementOperator();
//            substatement.Statements.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(substatement);

//            fd.Validate();
//        }
//        [Test]
//        public void FilterDefinition_Invalid_Validation_StartsWithAndOr()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new InOperator() { FieldName = "LastName", Values = new List<string>() { "Jimmy", "Borch" } });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            var substatement = new StatementOperator();
//            substatement.Statements.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(substatement);

//            Assert.Throws<Base.ValidationException>(fd.Validate);
//        }
//        [Test]
//        public void FilterDefinition_Invalid_Validation_EndsWithAndOr()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new InOperator() { FieldName = "LastName", Values = new List<string>() { "Jimmy", "Borch" } });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            var substatement = new StatementOperator();
//            substatement.Statements.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(substatement);
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });

//            Assert.Throws<Base.ValidationException>(fd.Validate);
//        }
//        [Test]
//        public void FilterDefinition_Invalid_Validation_DoubleAndOr()
//        {
//            FilterDefinition fd = new FilterDefinition();
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new CompareOperator() { FieldName = "LastName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Borch" });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            fd.EntityFilters.Add(new InOperator() { FieldName = "LastName", Values = new List<string>() { "Jimmy", "Borch" } });
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });
//            var substatement = new StatementOperator();
//            substatement.Statements.Add(new CompareOperator() { FieldName = "FirstName", Operator = CompareOperator.OperatorEnum.Equals, Value = "Jimmy" });
//            fd.EntityFilters.Add(substatement);
//            fd.EntityFilters.Add(new LogicOperator() { AndOr = LogicOperator.AndOrEnum.Or });

//            Assert.Throws<Base.ValidationException>(fd.Validate);
//        }
//    }
//}
