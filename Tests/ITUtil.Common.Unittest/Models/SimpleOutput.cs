﻿using ITUtil.Common.Command.Interfaces;

namespace ITUtil.Common.Unittest
{
    public class SimpleOutput : IDtoResponse
    {
        public string output { get; set; }
    }
}
